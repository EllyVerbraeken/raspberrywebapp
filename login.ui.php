<?php
    include('appcode/controller/login.php');
?>
<!---dialoogvenster LOGIN---------------------------------------------------------------------->
    <div id="loginpopup" class="form">
        <div>
            <form method="post"  
                action="<?php htmlentities($_SERVER['PHP_SELF']); ?>"  
                autocomplete="on"> 

                <h1>Login</h1>
                <div>
                    <a  id="close" class="close" href="javascript:void(0)">X</a>
                    <label for="username">Username:
                    </label>
                    <input id="username" name="username" required="required" type="text" />
                    <label for="password">Password:
                    </label>
                    <input id="password" name="password" required="required" type="password" />
                    <button type="submit" id="login" name="login">Login</button>
                </div>
            </form>
        </div>
    </div>
    <div id="fade"></div>