<?php
/**
   User Interface for the clients of E-Line : View
   @created 24 March 2014
   @lastmodified m May 2014
   @author Elly Verbraeken
   @version 1.0 
*/
    include('appcode/helpers/feedback.class.php');
    include('appcode/helpers/validate.class.php');
    include('appcode/dal/base.class.php'); 
    include('appcode/dal/client.class.php');
     /* always check if the user is already logged in 
    *  the object $membership is then available 
    *  elsewhere on the page
    */
    //Controller : list of clients for select box: admin only
    include('appcode/controller/getclients.php');
    include('login.ui.php');
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta http-equiv='X-UA-Compatible' content= edge" />
        <meta name="application-name" content="temperature-logging">
        <meta name="description" content="temperature logging in server rooms for monitoring and diagnostics">
        <meta name="keywords" content="temperature logging, server room monitoring">
        <meta name="author" content="Verbraeken Elly">
        <link type="text/css" rel="stylesheet" href="css/raspberry.css">
        <link type="text/css" rel="stylesheet" href="css/eline.client.ui.css">
        <link type="text/css" rel="stylesheet" href="css/iconfont.css">
        <link type="text/css" rel="stylesheet" href="css/media.css">
        <script type="text/javascript" src="javascript/useajax.js"></script>
        <script type="text/javascript" src="javascript/ajax.js"></script>
        <script type="text/javascript" src="javascript/chart.js"></script>
        <script type="text/javascript" src="javascript/login.js"></script>
        
        <title>E-Line Temperature Diagnostics</title>
        <script>
            var ClientIp = '';
            var canvasWidth;
            var canvasHeight;
            var canvas;

            window.onload = function () {
                canvasWidth = window.innerWidth / 2;
                //canvasHeight = window.innerHeight / 1.531;
                canvas = document.getElementById('canvas');
                canvas.width = canvasWidth;
                canvas.height = canvasWidth/2.143;

                getDataFromServer('average', '');
                init();
            }

            function SetClientIP(obj) {
                index = obj.selectedIndex;
                //if (index < 1) { ClientIp=''; }
                ClientIp = obj.options[index].value;
                getDataFromServer('average', ClientIp);
            }

            var loggedIn = <?php echo json_encode($logged);?>;
        </script>
        <noscript>
        This chart is unavailable because JavaScript is disabled on your computer. Please
        enable JavaScript and refresh this page to see the chart in action.
    </noscript>
    </head>
    
    <body >
        <header>
            <div class="room _4x1">
            <a href="index.php" id="home">
            <span class="icon-home"></span>
                    </a></div>
            <div class="room _2x1">
                <h1>E-Line Temperature Diagnostics</h1>
            </div>
            <div class="room _4x1">
                <a id="loginanchor" href="<?php echo $membership->GetLoginHyperlink();?>"> 
                    <span class="icon-lock"></span>
                    <span><?php echo $membership->GetLoginText();?></span>
                </a>
            </div>           
        </header>

        <div id="contentarea">
            
            <h1 id="title">Average temperatures</h1>
            <?php 
            if ($membership->isLoggedIn())
            {?>
            <select name="clients" id="clients" onchange="SetClientIP(this);">
                <option value="">Select client...</option>
             <?php                  
                    if( is_array( $result ) && (count($result) > 0) ) 
                    {                            
                        foreach ($result as $row)
                        {
                    ?> <option value="<?php echo $row['IPaddress'];?>"><?php echo $row['Name'];?></option>
                <?php   }
                    }?>
             </select>
            <?php    
            } ?>
            <div id="choice">
               <input type="button" value="Average" class="button" id="average" onclick="getDataFromServer(this.id,ClientIp)"/>
               <input type="button" value="Maximum" class="button" id="maximum" onclick="getDataFromServer(this.id,ClientIp)"/>
               <input type="button" value="Minimum" class="button" id="minimum" onclick="getDataFromServer(this.id, ClientIp)"/>
            </div>
            <div id="chart">
               <label class="rotate-text">Temperature ° Celsius</label>
               <canvas id="canvas" height="350" width="750" ></canvas>
               <label class="text">Days</label>
            </div>
        </div> <!--end content-area-->  
        <footer>
            <div id="vertical"></div>
            <div id="vertical2">
            <p>Copyright E-line KMO</p>
            </div>
            <div id="vertical"></div>
        </footer>
    </body>
</html>