<?php           /**
       PHP file 
       @created 17 march 2014
       @lastmodified 8 april 2014
       @author Elly Verbraeken
       @version 1.0 
    */
    include('appcode/helpers/feedback.class.php');
    include('appcode/helpers/validate.class.php');
    include ('appcode/dal/base.class.php');
     /* always check if the user is already logged in 
    *  the object $membership is then available 
    *  elsewhere on the page
    */
    include('login.ui.php');
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta name="application-name" content="temperature-logging">
        <meta name="description" content="temperature logging in server rooms for monitoring and diagnostics">
        <meta name="keywords" content="temperature logging, server room monitoring">
        <meta name="author" content="Verbraeken Elly">
        <link type="text/css" rel="stylesheet" href="css/raspberry.css">
        <link type="text/css" rel="stylesheet" href="css/index.css">
        <link type="text/css" rel="stylesheet" href="css/animate.css">
        <link type="text/css" rel="stylesheet" href="css/iconfont.css">
        <link type="text/css" rel="stylesheet" href="css/media.css">
        <script type="text/javascript" src="javascript/login.js"></script>
        <title>E-Line Temperature Logging</title>
        <script>
            window.onload = function () {
                init();
            }
            var loggedIn = <?php echo json_encode($logged);?>;
        </script>
        <noscript>
        The login is unavailable because JavaScript is disabled on your computer. Please
        enable JavaScript and refresh this page to see the login in action.
    </noscript>
    </head>
    <body>
        <header>
            <div class="room _4x1">
                <a href="index.php" id="home">
            <span class="icon-home"></span>
                    </a>
            </div>
            <div class="room _2x1">
                <h1>E-Line Temperature Logging</h1>
            </div>
            <div class="room _4x1">
                <a id="loginanchor" href="<?php echo $membership->GetLoginHyperlink();?>"> 
                    <span class="icon-lock"></span>
                    <span><?php echo $membership->GetLoginText();?></span>
                </a>
            </div>   
        </header>

        <div id="contentarea">
          <div id="tile1" class="tile hover">
          <a 
              <?php if ($membership->isLoggedIn())
                    {?>
                      href="eline.php"
             <?php  }
                    else
                    {?>
                      href=""
             <?php  }
                   ?> class="mask rotate-in">
             <h2>Real-time monitoring</h2>
             <p>Only available for administrators</p>
             <span class="action">Enter</span>
          </a>
          <h1>Monitoring</h1>
          <p>For current information</p>
          </div>

          <div id="tile2" class="tile hover">
          <a href="eline.client.php" class="mask rotate-in">
             <h2>Checkout history</h2>
             <p>For more information about past temperatures</p>
             <span class="action">Enter</span>
          </a>
          <h1>Diagnostics</h1>
          <p>For history information</p>
          </div>

        </div> <!--end content-area-->
             
        <footer>
            <div id="vertical"></div>
            <div id="vertical2">
            <p>Copyright E-line KMO</p>
            </div>
            <div id="vertical"></div>
        </footer>
    </body>
</html>