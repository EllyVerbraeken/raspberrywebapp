<?php
    /**
       Base class
       @created 4 April 2014
       @lastmodified 4 April 2014
       @author Elly Verbraeken
       @version 1.0 
    */
    namespace Temperature\Dal; 
        // namespace should represent what's code about

    class Base extends \Temperature\Helpers\Feedback
    {
            /*-----------Declaration of fields----------*/
         protected $pdo; 
         protected $rowCount; 
    
            /*-----------Getters and setters----------*/
        // if properies were public, they could be set from outside : not safe
        // so here are only public getters

        /** 
        * Get RowCount 
        * @return  the value in $rowCount field    
        */
        public function getRowCount() 
        { 
            return $this->rowCount; 
        } 
    
        /** 
        * Get PDO
        * @return  the value in $pdo field    
        */
         public function getPdo()
        {
            return $this->pdo;
        }

        /*-----------Constructor----------*/
        // using constructor of Feedback class
    
        /*-----------Methods----------*/

        /** 
        * Make connection with database
        * @return  Bool true if succeeded, false if not.    
        */ 
        public function connect() 
        { 
            // online database of dreamhost:
            $connectionstring = 'mysql:host=mysql.elly.inantwerpen.com;dbname=elly';
            $userName= 'ellyv';
            $password= 'peLLegrino';
            // for local host with WAMP server:
            //$userName= 'root';
            //$connectionstring='mysql:host=localhost;dbname=elly';
            //$password='';
            // sentinel
            $result = TRUE; 
            try 
            { 
                // The PDO class should be searched for in the root namespace
                // not in Temperature\Dal 
                $this->pdo = new \PDO($connectionstring, $userName, $password); 
               $this->pdo->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION); 
                // character encoding of the MYSQL connection string 
                // $this->pdo->exec('SET NAMES "utf8"'); 
                $this->feedback = 'Connected to database'; // this is enough information for a user
            } 
            catch (\PDOException $e) 
            { 
                $this->feedback = 'Unable to connect to the database server.'; 
                $this->errorMessage = $e->getMessage(); // technical error message
                $this->errorCode =$e->getCode();
                $result = FALSE; 
            } 
            return $result; 
        } 
    
        /** 
        * Close connection from database    
        */ 
         public function close() 
        {     
        $this->pdo = NULL;
        $feed = $this->feedback;
        $feed.=' Disconnected from database'.'<br/>';
        $this->feedback = $feed;
        }
   }
?>