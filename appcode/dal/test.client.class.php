<?php 
 /**
   Testing Client class
   @created 18 March 2014
   @lastmodified 18 March 2014
   @author Elly Verbraeken
   @version 1.0 
*/
include('../helpers/feedback.class.php');
include('../helpers/validate.class.php'); 
include('base.class.php'); 
include('client.class.php');
$result="";
$result1="";
$result2="";
$result3="";
$result4="";
$result5="";

$test = new Temperature\Dal\Client();
$test->setName('Daniel');
$test->setIpAddress('62.105.20.89');

$test->insert();
$result .= 'Feedback: ' . $test->getFeedback() . '<br>'; 
$result .= 'Foutmelding: ' . $test->getErrorMessage() . '<br>'; 
$result .= 'Foutcode: ' . $test->getErrorCode() . '<br>';
$result.= 'new id: ' . $test->getId() . '<br>'; 

$test->setIpAddress('65.30.50.55');
$test->update();
$result1 .= 'Feedback: ' . $test->getFeedback() . '<br>'; 
$result1 .= 'Foutmelding: ' . $test->getErrorMessage() . '<br>'; 
$result1 .= 'Foutcode: ' . $test->getErrorCode() . '<br>'; 
$result1 .= ' IP client : ' .$test->getIpAddress(). ' ';

$test->selectAll();
$result3 .= 'Feedback: ' . $test->getFeedback() . '<br>'; 
$result3 .= 'Foutmelding: ' . $test->getErrorMessage() . '<br>'; 
$result3 .= 'Foutcode: ' . $test->getErrorCode() . '<br>'; 

$test->setName('Elly');
$test->selectClient();
$result4 .= 'Feedback: ' . $test->getFeedback() . '<br>'; 
$result4 .= 'Foutmelding: ' . $test->getErrorMessage() . '<br>'; 
$result4 .= 'Foutcode: ' . $test->getErrorCode() . '<br>'; 
$result4 .= ' Naam client : ' .$test->getName(). ' ';

$test->selectClientById();
$result5 .= 'Feedback: ' . $test->getFeedback() . '<br>'; 
$result5 .= 'Foutmelding: ' . $test->getErrorMessage() . '<br>'; 
$result5 .= 'Foutcode: ' . $test->getErrorCode() . '<br>'; 
$result5 .= ' Naam client : ' .$test->getName(). ' ';

$test->delete();
$result2 .= 'Feedback: ' . $test->getFeedback() . '<br>'; 
$result2 .= 'Foutmelding: ' . $test->getErrorMessage() . '<br>'; 
$result2 .= 'Foutcode: ' . $test->getErrorCode() . '<br>'; 
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <title>Test client dal</title>
    </head>
    <body>
     
        <p>Test client klasse van DAL</p>
        <p>Foutmeldingen</p>
        <p><?php 
           $array = $test->getMessages();
           while (list($key, $val) = each($array)) 
           {
               while (list($keys, $values) = each($val)) 
               {?>
                 <p><?php echo $keys. ' : '.$values.'<br/>';?></p>
                <?php 
               }   
           } ?></p>
        <p>Insert methode</p>
            <?php echo $result;?> 
        <p>Update methode</p>
            <?php echo $result1;?> 
        <p>Select All methode</p>
            <?php echo $result3;?> 
        <p>Select Client methode</p>
            <?php echo $result4;?> 
        <p>Select Client By Id methode</p>
            <?php echo $result5;?> 
        <p>Delete methode</p>
        <?php echo $result2;?> 
    </body>
</html>