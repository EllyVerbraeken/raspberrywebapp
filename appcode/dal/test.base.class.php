<?php 
/**
   Testing Base class
   @created 5 March 2014
   @lastmodified 5 March 2014
   @author Elly Verbraeken
   @version 1.0 
*/
    include('../helpers/feedback.class.php');
    include ('base.class.php'); 
    $object = new Temperature\Dal\Base(); 
    $object->connect();     
?> 
 
<!DOCTYPE html> 
<html lang="en"> 
    <head> 
        <meta charset="utf-8" /> 
        <title>Test base dal klasse</title> 
    </head> 
    <body> 
        <p>Try to connect...</p> 
        <ul> 
            <li>Message: <?php echo $object->getFeedback();?></li> 
            <li>Error message: <?php echo $object->getErrorMessage();?></li> 
            <li>Error code: <?php echo $object->getErrorCode();?></li> 
        </ul> 
        <?php $object->close(); ?> 
        <p>Try to disconnect...</p> 
        <ul> 
            <li>Message: <?php echo $object->getFeedback();?></li> 
            <li>Error message: <?php echo $object->getErrorMessage();?></li> 
            <li>Error code: <?php echo $object->getErrorCode();?></li> 
        </ul> 
    </body> 
</html> 