<?php
    /**
       Client class
       @created 18 March 2014
       @lastmodified 18 March 2014
       @author Elly Verbraeken
       @version 1.0 
    */
    
    namespace Temperature\Dal;
     class Client extends \Temperature\Dal\Base 
     {
        /*-----------Declaration of fields----------*/
    
        private $id; 
        private $ipAddress ; 
        private $name; 
    
        /*-----------Getters and setters----------*/
    
        /** 
        * Get Id  
        * @return  the value in $id field    
        */
        public function getId() 
        { 
        return $this->id; 
        } 
    
        /** 
        * Get IpAddress  
        * @return  the value in $ipAddress field    
        */
        public function getIpAddress() 
        { 
        return $this->ipAddress; 
        } 
    
        /** 
        * Get Name 
        * @return  the value in $name field    
        */
        public function getName() 
        { 
        return $this->name; 
        } 
    
        /** 
        * Set Id   
        * @return  Bool true if not empty, false if empty.    
        */
        public function setId($value) 
        { 
            if(\Temperature\Helpers\Validate::isEmpty($value))
            {
                $this->messages[] =
                array ('Id' => 'Id is een verplicht veld.');
                $this->isError = TRUE;
                return FALSE;
            }
            else if (!is_numeric($value))
            {
                $this->messages[] =
                array ('Id' => 'Id moet een numerieke waarde hebben.');
                $this->isError = TRUE;
                return FALSE;
            }
            else 
            {
              $value = \Temperature\Helpers\Validate::stripTags($value);
              $this->id = $value; 
              return TRUE;
            }
        } 
    
         /** 
         * Set IpAddress
         * @return  Bool true if not empty, false if empty.   
         */
        public function setIpAddress($value) 
        { 
          if(\Temperature\Helpers\Validate::isIP($value))
        {
            $this->messages[] =
            array ('IPadres' => 'IP-adres is niet correct.');
            $this->isError = TRUE;
            return FALSE;}
            elseif (\Temperature\Helpers\Validate::isEmpty($value))
        {
            $this->messages[] =
            array ('IPadres' => 'IP-adres is verplicht. ');
            $this->isError = TRUE;
            return FALSE;}
        else 
        {
          $this->ipAddress = $value; 
          return TRUE;
        }
        } 
    
          /** 
          * Set Name  
          * @return  Bool true if not empty, false if empty.  
          */
        public function setName($value) 
        { 
           if(\Temperature\Helpers\Validate::isEmpty($value))
            {
                $this->messages[] =
                array ('Name' => 'Name is een verplicht veld.');
                $this->isError = TRUE;
                return FALSE;
            }
            else 
            {
              $value = \Temperature\Helpers\Validate::stripTags($value);
              $this->name = $value; 
              return TRUE;
            }
        } 
    
        /*-----------CRUD operations----------*/
        /*------------------------------------*/
    
        /*-----------INSERT----------*/
        /** 
        * Insert   
        * @return  Bool true if succeeded, false if not.    
        */
        public function insert() 
        { 
            
        if ($this->isError)
		{
			$this->errorMessage = 'Gegevens niet gevalideerd.';
			return FALSE;
		}
        $result = FALSE; // wachter
        $this->errorMessage = 'none'; 
        $this->errorCode = 'none';
        $this->feedback = 'none';
    
        if ($this->connect()) // use Connect method of base class
        { 
        try 
        { 
        // Prepare stored procedure call  
        // een command object C#
        $preparedStatement = $this->pdo-> 
        prepare('CALL ClientInsert(@pId, :pIpAddress, :pName)');
    
        // bind parameters to fields of client class
        $preparedStatement->bindParam(':pIpAddress', $this->ipAddress, \PDO::PARAM_STR, 80); 
        $preparedStatement->bindParam(':pName', $this->name,\PDO::PARAM_STR, 80); 
    
        // execute SQL statement
        $preparedStatement->execute(); 
        $this->feedback = "Client {$this->name} is toegevoegd.";
        // fetch the output : result of query : output parameter 
        $this->setId($this->pdo->query('select @pId')->fetchColumn()); 
        //$this->feedback = 'Rij met id <b> ' . $this->GetId() .  
        //'</b> is toegevoegd.'; 
        $result = TRUE; 
        } 
        catch (\PDOException $e) 
        { 
        $this->feedback = "Client {$this->name} is niet toegevoegd."; 
        $this->errorMessage = 'Fout: ' . $e->getMessage(); 
        $this->errorCode = $e->getCode(); 
        } 
        $this->close(); 
        } // end if connect
        return $result; 
        }// end insert
    
        /*-----------UPDATE----------*/
       // Update client by Name
        /** 
        * Update   
        * @return Bool true if succeeded, false if not.    
        */
        public function update() 
    { 
        
        if ($this->isError)
		{
			$this->errorMessage = 'Gegevens niet gevalideerd.';
			return FALSE;
		}
        $result = FALSE; 
        $this->errorCode = 'none'; 
        $this->errorMessage = 'none'; 
        $this->feedback = 'none'; 
    
        if ($this->connect()) 
        { 
            try 
            { 
                $preparedStatement = $this->pdo-> 
                    prepare('CALL ClientUpdate(:pIpAddress, :pName)'); 
                $preparedStatement->bindParam(':pName',$this->name,\PDO::PARAM_STR, 80); 
                $preparedStatement->bindParam(':pIpAddress',$this->ipAddress,\PDO::PARAM_STR, 80); 
                $preparedStatement->execute(); 
                $this->feedback = "Client {$this->name} is geupdated."; 
                $result = TRUE; 
            } 
            catch (\PDOException $e) 
            { 
                $this->feedback = "Client {$this->name} is niet geupdated."; 
                $this->errorMessage = $e->getMessage(); 
                $this->errorCode = $e->getCode(); 
            } 
            //$this->close(); 
        } 
        return $result; 
    } 
        /*-----------DELETE----------*/
        // Remove client by Name
        /** 
        * Delete   
        * @return 1 if succeeded, false if not.    
        */
        public function delete() 
        { 
            
        if ($this->isError)
		{
			$this->errorMessage = 'Gegevens niet gevalideerd.';
			return FALSE;
		}
        $this->errorMessage = 'none'; 
        $this->errorCode = 'none'; 
        $result = FALSE; 
        if ($this->connect()) 
        { 
        try 
        { 
        // Prepare stored procedure call 
        $preparedStatement = $this->pdo-> 
        prepare('CALL ClientDelete(:pName)'); 
    
        $preparedStatement->bindParam(':pName', $this->name, \PDO::PARAM_STR, 80); 
        $preparedStatement->execute(); 
        // fetch the output 
        $result = $preparedStatement->rowCount(); 
        if ($result) 
        { 
        $this->feedback = 'Gegevens van ' . $this->getName() .  
        ' zijn gedeleted.'; 
        } 
        else
                    {
                        $this->feedback = 'Gegevens van ' . $this->getName() .  
        ' zijn niet gevonden en dus niet gedeleted.';
                    }
                }
                catch (\PDOException $e)
                {
                    $this->feedback = 'fout => Gegevens zijn niet gedeleted.';
                    $this->errorMessage = 'Fout: ' . $e->getMessage();
                    $this->errorCode = $e->getCode();
                }
                $this->close();
            }
            return $result;
        }
    
        /*-----------SELECT----------*/
        // Select all clients
        /** 
        * SelectAll   
        * @return array if succeeded, false if not.    
        */
        public function selectAll()
        {
            $this->errorMessage = 'none';
            $this->errorCode = 'none';
            $result = FALSE;
            if ($this->connect())
            {
                try
                {
                    // Prepare stored procedure call
                    $preparedStatement = $this->pdo->prepare('CALL ClientSelectAll()');
                    $preparedStatement->execute();
                    $this->rowCount = $preparedStatement->rowCount();
                    if ($result = $preparedStatement->fetchAll())
                    {
                        $this->feedback = 'Alle clienten zijn ingelezen.';
                    }
                    else
                    {
                        $this->feedback = 'De clienten zijn NIET ingelezen.';
                    }
                }
                catch (\PDOException $e)
                {
                    $this->feedback = 'Er is iets foutgelopen bij het inlezen van de client tabel.';
                    $this->errorMessage = 'Fout: ' . $e->getMessage();
                    $this->errorCode = $e->getCode();
                    $this->rowCount = -1;
                }
                $this->close();
            }
                return $result;
            }
    
        // Select client by IpAddress
        /** 
        * Select  
        * @return true if succeeded, false if not.    
        */
        public function selectClient()
        {
            
        if ($this->isError)
		{
			$this->errorMessage = 'Gegevens niet gevalideerd.';
			return FALSE;
		}
            $this->errorMessage = 'none';
            $this->errorCode = 'none';
            $result = FALSE;
            if ($this->connect())
            {
                try
                {
                    // Prepare stored procedure call
                    $preparedStatement = $this->pdo->prepare('CALL ClientSelectClient(:pIpAddress)');
                     $preparedStatement->bindParam(':pIpAddress', $this->ipAddress, \PDO::PARAM_STR, 80);
                    $preparedStatement->execute();
                     // fetch the output 
            if ($oneRow = $preparedStatement->fetch(\PDO::FETCH_ASSOC)) 
            { 
                $this->name=$oneRow['Name']; 
                $this->id=$oneRow['Id'];
                $this->feedback = 'De client is ingelezen.';
            } 
            else
            {
                $this->feedback = 'De client is NIET ingelezen.';
            }       
                    $result= TRUE;
                }
                catch (\PDOException $e)
                {
                    $this->feedback = 'Er is iets foutgelopen bij het inlezen van de client tabel.';
                    $this->errorMessage = 'Fout: ' . $e->getMessage();
                    $this->errorCode = $e->getCode();
                    $this->rowCount = -1;
                }
                $this->close();
            }
                return $result;
            }

            // Select client by Id
        /** 
        * Select  
        * @return true if succeeded, false if not.    
        */
        public function selectClientById()
        {
            
        if ($this->isError)
		{
			$this->errorMessage = 'Gegevens niet gevalideerd.';
			return FALSE;
		}
            $this->errorMessage = 'none';
            $this->errorCode = 'none';
            $result = FALSE;
            if ($this->connect())
            {
                try
                {
                    // Prepare stored procedure call
                    $preparedStatement = $this->pdo->prepare('CALL ClientSelectById(:pId)');
                     $preparedStatement->bindParam(':pId', $this->id, \PDO::PARAM_INT);
                    $preparedStatement->execute();
                     // fetch the output 
            if ($oneRow = $preparedStatement->fetch(\PDO::FETCH_ASSOC)) 
            { 
                $this->name=$oneRow['Name']; 
                $this->ipAddress=$oneRow['IPaddress'];
            } 
                    $this->feedback = 'De client is ingelezen.';
                    $result= TRUE;
                }
                catch (\PDOException $e)
                {
                    $this->feedback = 'Er is iets foutgelopen bij het inlezen van de client tabel.';
                    $this->errorMessage = 'Fout: ' . $e->getMessage();
                    $this->errorCode = $e->getCode();
                    $this->rowCount = -1;
                }
                $this->close();
            }
                return $result;
            }
     }
?>