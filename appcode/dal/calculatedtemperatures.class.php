<?php
    /**
   Calculated temperatures class
   @created 6 March 2014
   @lastmodified 13 March 2014
   @author Elly Verbraeken
   @version 1.0 
*/
namespace Temperature\Dal;
 class CalculatedTemperatures extends \Temperature\Dal\Base 
 {
     /*-----------Declaration of fields----------*/
    private $id; 
    private $client; 
    private $minimum; 
    private $maximum;
    private $average;
    private $day;

    /*-----------Getters and setters----------*/

    /** 
    * Get Id  
    * @return  the value in $id field    
    */
    public function getId() 
    { 
    return $this->id; 
    } 
    
    /** 
    * Get Client  
    * @return  the value in $client field    
    */
    public function getClient() 
    { 
    return $this->client; 
    } 
    
    /** 
    * Get Minimum  
    * @return  the value in $minimum field    
    */
    public function getMinimum() 
    { 
    return $this->minimum; 
    }

    /** 
    * Get Maximum
    * @return  the value in $maximum field    
    */
    public function getMaximum() 
    { 
    return $this->maximum; 
    }

    /** 
    * Get Average  
    * @return  the value in $average field    
    */
    public function getAverage() 
    { 
    return $this->average; 
    }

    /** 
    * Get Day  
    * @return  the value in $day field    
    */
    public function getDay() 
    { 
    return $this->day; 
    }

     /** 
     * Set Id   
     * @return  Bool true als niet leeg, false als leeg.    
     */
    public function setId($value) 
    { 
        if(\Temperature\Helpers\Validate::isEmpty($value))
            {
                $this->messages[] =
                array ('Id' => 'Id is een verplicht veld.');
                $this->isError = TRUE;
                return FALSE;
            }
            else if (!is_numeric($value))
            {
                $this->messages[] =
                array ('Id' => 'Id moet een numerieke waarde hebben.');
                $this->isError = TRUE;
                return FALSE;
            }
            else 
            {
              $value = \Temperature\Helpers\Validate::stripTags($value);
              $this->id = $value; 
              return TRUE;
            }
    } 
       
        /** 
        * Set Client
        * @return  Bool true als niet leeg, false als leeg.    
        */
    public function setClient($value) 
    { 
    if(\Temperature\Helpers\Validate::isEmpty($value))
            {
                $this->messages[] =
                array ('ClientId' => 'ClientId is een verplicht veld.');
                $this->isError = TRUE;
                return FALSE;
            }
            else if (!is_numeric($value))
            {
                $this->messages[] =
                array ('ClientId' => 'ClientId moet een numerieke waarde hebben.');
                $this->isError = TRUE;
                return FALSE;
            }
            else 
            {
              $value = \Temperature\Helpers\Validate::stripTags($value);
              $this->client = $value; 
              return TRUE;
            }
    } 
       
        /** 
        * Set Maximum
        * @return  Bool true als niet leeg, false als leeg.    
        */
    public function setMaximum($value) 
    { 
    if(\Temperature\Helpers\Validate::isEmpty($value))
            {
                $this->messages[] =
                array ('Maximum' => 'Maximum is een verplicht veld.');
                $this->isError = TRUE;
                return FALSE;
            }
            else if (!is_numeric($value))
            {
                $this->messages[] =
                array ('Maximum' => 'Maximum moet een numerieke waarde hebben.');
                $this->isError = TRUE;
                return FALSE;
            }
            else 
            {
              $value = \Temperature\Helpers\Validate::stripTags($value);
              $this->maximum = $value; 
              return TRUE;
            }
    } 
       
        /** 
        * Set Minimum
        * @return  Bool true als niet leeg, false als leeg.    
        */
    public function setMinimum($value) 
    { 
    if(\Temperature\Helpers\Validate::isEmpty($value))
            {
                $this->messages[] =
                array ('Minimum' => 'Minimum is een verplicht veld.');
                $this->isError = TRUE;
                return FALSE;
            }
            else if (!is_numeric($value))
            {
                $this->messages[] =
                array ('Minimum' => 'Minimum moet een numerieke waarde hebben.');
                $this->isError = TRUE;
                return FALSE;
            }
            else 
            {
              $value = \Temperature\Helpers\Validate::stripTags($value);
              $this->minimum = $value; 
              return TRUE;
            }
    } 
    
        /** 
        * Set Average
        * @return  Bool true als niet leeg, false als leeg.    
        */
    public function setAverage($value) 
    { 
    if(\Temperature\Helpers\Validate::isEmpty($value))
            {
                $this->messages[] =
                array ('Average' => 'Average is een verplicht veld.');
                $this->isError = TRUE;
                return FALSE;
            }
            else if (!is_numeric($value))
            {
                $this->messages[] =
                array ('Average' => 'Average moet een numerieke waarde hebben.');
                $this->isError = TRUE;
                return FALSE;
            }
            else 
            {
              $value = \Temperature\Helpers\Validate::stripTags($value);
              $this->average = $value; 
              return TRUE;
            }
    } 

        /** 
        * Set Day
        * @return  Bool true als niet leeg, false als leeg.    
        */
    public function setDay($value) 
    { 
     if(\Temperature\Helpers\Validate::isEmpty($value))
            {
                $this->messages[] =
                array ('Day' => 'Day is een verplicht veld.');
                $this->isError = TRUE;
                return FALSE;
            }
            else 
            {
              $value = \Temperature\Helpers\Validate::stripTags($value);
              $this->day = $value; 
              return TRUE;
            }
    } 

    /*-----------CRUD operations----------*/
    /*------------------------------------*/

    /*-----------INSERT----------*/
    /** 
    * Insert   
    * @return  Bool true if succeeded, false if not.    
    */
    public function insert()
    { 
        if ($this->isError)
		{
			$this->errorMessage = 'Gegevens niet gevalideerd.';
			return FALSE;
		}
        $result = FALSE; // waiter
        $this->errorMessage = 'none'; 
        $this->errorCode = 'none';
        $this->feedback = 'none';
     
        if ($this->connect()) // use Connect method of base class
        { 
            try 
            {   $preparedStatement = $this->pdo-> 
                prepare('CALL CalculatedTemperaturesInsert(@pId, :pClient, :pMinimum, :pMaximum, :pAverage, :pDay)');
   
                // bind parameters to fields of CalculatedTemperatures class
                $preparedStatement->bindParam(':pClient', $this->client, \PDO::PARAM_INT); // : : static
                $preparedStatement->bindParam(':pMinimum', $this->minimum,\PDO::PARAM_STR, 10); 
                $preparedStatement->bindParam(':pMaximum', $this->maximum, \PDO::PARAM_STR, 10);  
                $preparedStatement->bindParam(':pAverage', $this->average, \PDO::PARAM_STR, 10); 
                $preparedStatement->bindParam(':pDay', $this->day, \PDO::PARAM_STR, 10);     
  
                // execute SQL statement
                $preparedStatement->execute(); 
                $this->feedback = "De temperaturen, {$this->minimum}, {$this->maximum}, {$this->average} zijn toegevoegd.";
                // fetch the output : result of query : output parameter 
                $this->setId($this->pdo->query('select @pId')->fetchColumn()); 
                //$this->feedback = 'Rij met id <b> ' . $this->GetId() .  
                //'</b> is toegevoegd.'; 
                $result = TRUE; 
            } 
            catch (\PDOException $e) 
            { 
                $this->feedback = "De temperaturen voor {$this->client} zijn niet toegevoegd."; 
                $this->errorMessage = 'Fout: ' . $e->getMessage(); 
                $this->errorCode = $e->getCode(); 
            } 
            $this->close(); 
        } // end if connect
        return $result; 
    }// end insert

    /*-----------UPDATE----------*/
    // No Update , table is filled by PHP

    /*-----------DELETE----------*/
    // Empty table after certain date
    /** 
    * Delete   
    * @return 1 if succeeded, false if not.    
    */
    public function delete($daydelete) 
    { 
        if ($this->isError)
		{
			$this->errorMessage = 'Gegevens niet gevalideerd.';
			return FALSE;
		}
        $this->errorMessage = 'none'; 
        $this->errorCode = 'none'; 
        $result = FALSE; 
        if ($this->connect()) 
        { 
            try 
            { 
                // Prepare stored procedure call 
                $preparedStatement = $this->pdo-> 
                prepare('CALL CalculatedTemperaturesDelete(:pDay)'); 
   
                $preparedStatement->bindParam(':pDay', $daydelete, \PDO::PARAM_STR, 80); 
                $preparedStatement->execute(); 
                // fetch the output 
                $result = $preparedStatement->rowCount(); 
                if ($result) 
                { 
                    $this->feedback = 'Temperaturen voor ' . $daydelete .  
                    ' zijn gedeleted.'; 
                } 
                else
		        {
		            $this->feedback = 'Temperaturen voor ' . $daydelete.' zijn niet gevonden en dus niet gedeleted.';
		        }
			}
			catch (\PDOException $e)
			{
				$this->feedback = 'fout => Temperaturen zijn niet gedeleted.';
				$this->errorMessage = 'Fout: ' . $e->getMessage();
				$this->errorCode = $e->getCode();
			}
			$this->close();
		}
		return $result;
	}

     /*-----------SELECTAverage----------*/
    // Select average temperatures for one client
    /** 
    * SelectAverage   
    * @return array if succeeded, false if not.    
    */
    public function selectAverage()
	{
        if ($this->isError)
		{
			$this->errorMessage = 'Gegevens niet gevalideerd.';
			return FALSE;
		}
		$this->errorMessage = 'none';
		$this->errorCode = 'none';
		$result = FALSE;
		if ($this->connect())
		{
			try
			{
				// Prepare stored procedure call
				$preparedStatement = $this->pdo->prepare('CALL CalculatedTemperaturesSelectAverage(:pClient)');
                $preparedStatement->bindParam(':pClient', $this->client, \PDO::PARAM_INT); 
				$preparedStatement->execute();
				$this->rowCount = $preparedStatement->rowCount();
				if ($result = $preparedStatement->fetchAll())
				{
					$this->feedback = 'De gemidddelde temperaturen van ' . $this->getClient() .' zijn ingelezen.';                    
				}
				else
				{
					$this->feedback = 'De gemiddelde temperaturen van ' . $this->getClient() .' zijn NIET ingelezen.';
                    $result = FALSE;
				}
			}
			catch (\PDOException $e)
			{
				$this->feedback = 'Er is iets foutgelopen bij het inlezen van de gemiddelde temperaturen.';
				$this->errorMessage = 'Fout: ' . $e->getMessage();
				$this->errorCode = $e->getCode();
				$this->rowCount = -1;
			}
			$this->close();
        }
			return $result;
		}
     /*-----------SELECTMinimum----------*/
    // Select minimum temperatures for 1 client
    /** 
    * SelectMinimum   
    * @return array if succeeded, false if not.    
    */
    public function selectMinimum()
	{
        if ($this->isError)
		{
			$this->errorMessage = 'Gegevens niet gevalideerd.';
			return FALSE;
		}
		$this->errorMessage = 'none';
		$this->errorCode = 'none';
		$result = FALSE;
		if ($this->connect())
		{
			try
			{
				// Prepare stored procedure call
				$preparedStatement = $this->pdo->prepare('CALL CalculatedTemperaturesSelectMinimum(:pClient)');
                $preparedStatement->bindParam(':pClient', $this->client, \PDO::PARAM_INT); 
				$preparedStatement->execute();
				$this->rowCount = $preparedStatement->rowCount();
				if ($result = $preparedStatement->fetchAll())
				{
					$this->feedback = 'De minimum temperaturen van ' . $this->getClient() .' zijn ingelezen.';
				}
				else
				{
					$this->feedback = 'De minimmum temperaturen van ' . $this->getClient() .' zijn NIET ingelezen.';
                    $result = FALSE;
				}
			}
			catch (\PDOException $e)
			{
				$this->feedback = 'Er is iets foutgelopen bij het inlezen van de minimum temperaturen.';
				$this->errorMessage = 'Fout: ' . $e->getMessage();
				$this->errorCode = $e->getCode();
				$this->rowCount = -1;
			}
			$this->close();
        }
			return $result;
		}
     /*-----------SELECTMaximum----------*/
    // Select maximum temperatures for 1 client
    /** 
    * SelectMaximum   
    * @return array if succeeded, false if not.    
    */

    public function selectMaximum()
	{
        if ($this->isError)
		{
			$this->errorMessage = 'Gegevens niet gevalideerd.';
			return FALSE;
		}
		$this->errorMessage = 'none';
		$this->errorCode = 'none';
		$result = FALSE;
		if ($this->connect())
		{
			try
			{
				// Prepare stored procedure call
				$preparedStatement = $this->pdo->prepare('CALL CalculatedTemperaturesSelectMaximum(:pClient)');
                $preparedStatement->bindParam(':pClient', $this->client, \PDO::PARAM_INT); 
				$preparedStatement->execute();
				$this->rowCount = $preparedStatement->rowCount();
				if ($result = $preparedStatement->fetchAll())
				{
					$this->feedback = 'De maximum temperaturen van ' . $this->getClient() .' zijn ingelezen.';
				}
				else
				{
					$this->feedback = 'De maximum temperaturen van ' . $this->getClient() .' zijn NIET ingelezen.';
                    $result = FALSE;
				}
			}
			catch (\PDOException $e)
			{
				$this->feedback = 'Er is iets foutgelopen bij het inlezen van de maximum temperaturen.';
				$this->errorMessage = 'Fout: ' . $e->getMessage();
				$this->errorCode = $e->getCode();
				$this->rowCount = -1;
			}
			$this->close();
        }
			return $result;
		}
 }
?>