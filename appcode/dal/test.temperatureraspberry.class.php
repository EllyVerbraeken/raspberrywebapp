<?php
/**
   Testing TemperatureRaspberry class
   @created 6 March 2014
   @lastmodified 17 March 2014
   @author Elly Verbraeken
   @version 1.0 
*/
include('../helpers/feedback.class.php');
include('../helpers/validate.class.php'); 
include('base.class.php'); 
include('temperatureraspberry.class.php');
$result="";
$result1="";
$result2="";
$result3="";
$degree= 17.06;

$test = new Temperature\Dal\TemperatureRaspberry();
$test->setClient(11);
$test->setDegree($degree);
$test->setWarning('NO');
//$test->setTime($day);

$test->insert();
$result .= 'Feedback: ' . $test->getFeedback() . '<br>'; 
$result .= 'Foutmelding: ' . $test->getErrorMessage() . '<br>'; 
$result .= 'Foutcode: ' . $test->getErrorCode() . '<br>';
$result.= 'new id: ' . $test->getId() . '<br>'; 

$test->selectAllTempByClient();
$result1 .= 'Feedback: ' . $test->getFeedback() . '<br>'; 
$result1 .= 'Foutmelding: ' . $test->getErrorMessage() . '<br>'; 
$result1 .= 'Foutcode: ' . $test->getErrorCode() . '<br>'; 

$test->selectRecentTempByClients();
$result3 .= 'Feedback: ' . $test->getFeedback() . '<br>'; 
$result3 .= 'Foutmelding: ' . $test->getErrorMessage() . '<br>'; 
$result3 .= 'Foutcode: ' . $test->getErrorCode() . '<br>'; 

$test->delete();
$result2 .= 'Feedback: ' . $test->getFeedback() . '<br>'; 
$result2 .= 'Foutmelding: ' . $test->getErrorMessage() . '<br>'; 
$result2 .= 'Foutcode: ' . $test->getErrorCode() . '<br>'; 
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <title>Test temperature raspberry dal</title>
    </head>
    <body>
     
        <p>Test temperature raspberry klasse van DAL</p>
        <p>Foutmeldingen</p>
        <p><?php 
           $array = $test->getMessages();
           while (list($key, $val) = each($array)) 
           {
               while (list($keys, $values) = each($val)) 
               {?>
                 <p><?php echo $keys. ' : '.$values.'<br/>';?></p>
                <?php 
               }   
           } ?></p>

        <p>Insert methode</p>
            <?php echo $result;?> 
        <p>Select methode</p>
            <?php echo $result1;?> 
        <p>Select Recent methode</p>
            <?php echo $result3;?> 
        <p>Delete methode</p>
        <?php echo $result2;?> 
    </body>
</html>