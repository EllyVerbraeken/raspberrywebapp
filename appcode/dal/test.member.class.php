<?php 
/**
   Testing Member class
   @created 7 May 2014
   @lastmodified 7 May 2014
   @author Elly Verbraeken
   @version 1.0 
*/
include('../helpers/feedback.class.php');
include ('../lib/password.php');
include('../helpers/validate.class.php'); 
include('base.class.php'); 
include ('member.class.php'); 
$member = new Temperature\Dal\Member(); 
$startTimeStamp = date("Y-m-d H:i:s"); 
echo 'start: ' . $startTimeStamp. '<br>'; 
$member->setUserName('Dirk'); 
$member->setEmail('stalcobronc@hotmail.com'); 
$member->setPassword('admin'); 

//insert method 
$testing = $member->insert(); 

echo 'new id: ' . $member->getId() . '<br>'; 
echo 'Feedback: ' . $member->getFeedback(). '<br>'; 
echo 'Error message: ' . $member->getErrorMessage(). '<br>'; 
echo 'Error code: ' . $member->getErrorCode(). '<br>'; 

$member->selectByUserNamePassword();
echo 'Test procedure SelectByUserName:<br>'; 
echo 'id: ' . $member->getId() .'<br>'; 
echo 'UserName: ' . $member->getUserName(). '<br>'; 
echo 'email: ' .  $member->getEmail(). '<br>'; 
echo 'Feedback: ' . $member->getFeedback(). '<br>'; 
echo 'Error message: ' . $member->getErrorMessage(). '<br>'; 
echo 'Error code: ' . $member->getErrorCode(). '<br>'; 

$endTimeStamp = date("Y-m-d H:i:s"); 
echo 'end: ' . $endTimeStamp. '<br>'; 
?> 