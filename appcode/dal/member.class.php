<?php
/**
       Member class
       @created 7 May 2014
       @lastmodified 10 May 2014
       @author Elly Verbraeken
       @version 1.0 
*/
namespace Temperature\Dal; 
// using password.php
class Member extends \Temperature\Dal\Base 
{
 /*-----------Declaration of fields----------*/
private $id; 
private $userName; 
private $email; 
private $password; 

/*-----------Getters and setters----------*/
    
/** 
* Get Id  
* @return  the value in $id field    
*/
public function getId() 
{ 
  return $this->id; 
} 
/** 
* Get UserName  
* @return  the value in $userName field    
*/ 
public function getUserName() 
{ 
  return $this->userName; 
} 
/** 
* Get Email  
* @return  the value in $email field    
*/ 
public function getEmail() 
{ 
  return $this->email; 
} 
/** 
* Verify a password against a hash using a timing attack resistant approach
* @return boolean If the password matches the hash    
*/ 
public function verifyPassword($value) 
{ 
  return password_verify($value, $this->password); 
} 
/** 
* Get Password  
* @return  the value in $password field    
*/ 
public function getPassword() 
{ 
  return $this->password; 
} 
/** 
* Set Id   
* @return  Bool true if not empty, false if empty.    
*/
public function setId($value) 
{ 
    if(\Temperature\Helpers\Validate::isEmpty($value))
    {
        $this->messages[] =
        array ('Id' => 'Id is een verplicht veld.');
        $this->isError = TRUE;
        return FALSE;
    }
    else if (!is_numeric($value))
    {
        $this->messages[] =
        array ('Id' => 'Id moet een numerieke waarde hebben.');
        $this->isError = TRUE;
        return FALSE;
    }
    else 
    {
        $value = \Temperature\Helpers\Validate::stripTags($value);
        $this->id = $value; 
        return TRUE;
    }
} 
/** 
* Set Name  
* @return  Bool true if not empty, false if empty.  
*/ 
public function setUserName($value) 
{ 
    if(\Temperature\Helpers\Validate::isEmpty($value))
    {
        $this->messages[] =
        array ('UserName' => '>UserName is een verplicht veld.');
        $this->isError = TRUE;
        return FALSE;
    }
    else 
    {
        $value = \Temperature\Helpers\Validate::stripTags($value);
        $this->userName = $value; 
        return TRUE;
    }
} 
/** 
* Set Email
* @return  Bool true if not empty, false if empty.   
*/ 
public function setEmail($value) 
{ 
 if(\Temperature\Helpers\Validate::email($value))
    {
        $this->messages[] =
        array ('Email' => 'Email is niet correct.');
        $this->isError = TRUE;
        return FALSE;
    }
 elseif (\Temperature\Helpers\Validate::isEmpty($value))
    {
        $this->messages[] =
        array ('Email' => 'Email is verplicht. ');
        $this->isError = TRUE;
        return FALSE;
    }
 else 
    {
        $this->email = $value; 
        return TRUE;
    } 
} 
/**
* Hash the password using the specified algorithm
* @return string|false The hashed password, or false on error.
*/ 
public function setPassword($value) 
{ 
$this->password = password_hash($value,  PASSWORD_DEFAULT); 
} 

/*-----------CRUD operations----------*/
/*------------------------------------*/
    
/*-----------INSERT----------*/
/** 
* Insert   
* @return  Bool true if succeeded, false if not.    
*/
public function insert() 
{ 
    $result = FALSE; 
    $this->errorMessage = 'none'; 
    $this->errorCode = 'none'; 
    if ($this->connect()) 
    { 
        try 
        { 
            // Prepare stored procedure call 
            $preparedStatement = $this->pdo-> 
            prepare('CALL MemberInsert(@pId,:pUserName,:pEmail,:pPassword)'); 
            // no getter method, bindParam requires  
              // a reference variable 
            // if you want to use a method, use then bindValue 
            $preparedStatement->bindParam( ':pUserName', $this->userName, \PDO::PARAM_STR, 80); 
            $preparedStatement->bindParam(':pEmail', $this->email, \PDO::PARAM_STR, 80); 
            $preparedStatement->bindParam(':pPassword', $this->password, \PDO::PARAM_STR, 255); 

            $succes = $preparedStatement->execute(); 
            // fetch the output 
            if ($succes == 1)
            {
                $this->setId($this->pdo->query('select @pId')->fetchColumn()); 
                $this->feedback = 'Rij met id <b> ' . $this->getId() .  
                '</b> is toegevoegd.'; 
                $result = TRUE; 
            }
            else
            {
                $this->feedback = 'Rij is niet toegevoegd.Insert mislukt.';
                $sqlErrorInfo= $preparedStatement->errorInfo();
                $this->errorCode = $sqlErrorInfo[0];
                $this->errorMessage = $sqlErrorInfo[2];
                $result=FALSE;
            }
        } 
        catch (\PDOException $e) 
        { 
            $this->feedback = 'Rij is niet toegevoegd.'; 
            $this->errorMessage = 'Fout: ' . $e->getMessage(); 
            $this->errorCode = $e->getCode(); 
        } 
        $this->close();  
    } 
    return $result;
} 

/*-----------UPDATE----------*/
/** 
* Update   
* @return Bool true if succeeded, false if not.    
*/
public function update() 
{ 
    $this->errorMessage = 'none'; 
    $this->errorCode = 'none'; 
    $result = FALSE; 
    if ($this->connect()) 
    { 
        try 
        { 
            // Prepare stored procedure call 
            $preparedStatement = $this->pdo-> 
            prepare('CALL MemberUpdate(:pId, :pUserName, :pEmail, :pPassword)'); 
            // no getter method, bindParam requires a reference variable 
            // if you want to use a method, use then binndValue 
            $preparedStatement->bindParam(':pId', $this->id, \PDO::PARAM_INT); 
            $preparedStatement->bindParam(':pUserName', $this->userName, \PDO::PARAM_STR, 80); 
            $preparedStatement->bindParam(':pEmail', $this->email, \PDO::PARAM_STR, 80); 
            $preparedStatement->bindParam(':pPassword', $this->password, \PDO::PARAM_STR, 255); 

            $preparedStatement->execute(); 
            // fetch the output 
            $result = $preparedStatement->rowCount(); 
            if ($result) 
            { 
                $this->feedback = 'Rij met id <b> ' . $this->getId() .  
                '</b> is gewijzigd.'; 
            } 
            else 
            { 
                $this->feedback = 'Rij met id <b> ' . $this->getId() .  
                '</b> is niet gevonden en dus niet gewijzigd.'; 
            } 
        } 
        catch (\PDOException $e) 
        { 
            $this->feedback = 'fout => rij is niet gewijzigd.'; 
            $this->errorMessage = 'Fout: ' . $e->getMessage(); 
            $this->errorCode = $e->getCode(); 
        } 
        $this->close(); 
    } 
    return $result; 
} 

/*-----------DELETE----------*/
/** 
* Delete   
* @return 1 if succeeded, false if not.    
*/
public function delete() 
{ 
    $this->errorMessage = 'none'; 
    $this->errorCode = 'none'; 
    $result = FALSE; 
    if ($this->connect()) 
    { 
        try 
        { 
            // Prepare stored procedure call 
            $preparedStatement = $this->pdo-> 
            prepare('CALL MemberDelete(:pId)'); 
            // no getter method, bindParam requires a reference variable 
            // if you want to use a method, use then binndValue 
            $preparedStatement->bindParam(':pId', $this->id, \PDO::PARAM_INT); 
            $preparedStatement->execute(); 
            // fetch the output 
            $result = $preparedStatement->rowCount(); 
            if ($result) 
            { 
                $this->feedback = 'Rij met id <b> ' . $this->getId() .  
                '</b> is gedeleted.'; 
            } 
            else 
            { 
                $this->feedback = 'Rij met id <b> ' . $this->getId() .  
                '</b> is niet gevonden en dus niet gedeleted.'; 
            } 
        } 
        catch (\PDOException $e) 
        { 
            $this->feedback = 'fout => rij is niet gedeleted.'; 
            $this->errorMessage = 'Fout: ' . $e->getMessage(); 
            $this->errorCode = $e->getCode(); 
        } 
        $this->close(); 
    } 
    return $result; 
} 

/*-----------SELECT----------*/
// Select member by UserName and Password
/** 
* Select  
* @return array if succeeded, false if not.    
*/
public function selectByUserNamePassword() 
{ 
    $this->errorMessage = 'none'; 
    $this->errorCode = 'none'; 
    $result = FALSE; 
    if ($this->connect()) 
    { 
        try 
        { 
            // Prepare stored procedure call 
            $preparedStatement = $this->pdo-> 
            prepare("CALL MemberSelectByNameAndPassword(:pUserName, :pPassword)"); 
            // no getter method, bindParam requires a reference variable 
            // if you want to use a method, use then bindValue 
            $preparedStatement->bindParam(':pUserName', $this->userName, \PDO::PARAM_STR, 80); 
            $preparedStatement->bindParam(':pPassword', $this->password, \PDO::PARAM_STR, 255);
            $preparedStatement->execute(); 
            $this->rowCount = $preparedStatement->rowCount(); 
            // fetch the output 
            if ($result = $preparedStatement->fetchAll()) 
            { 
                $this->feedback .= "{$preparedStatement->rowCount()} rij(en) met $this->userName in tabel Member gevonden."; 
                $array = $result[0]; 
                $this->id= $array['Id']; 
                $this->userName= $array['UserName']; 
                $this->email= $array['Email']; 
            } 
            else 
            { 
                $this->feedback .= "Geen rijen met $this->userName in tabel Member gevonden."; 
                $sQLErrorInfo = $preparedStatement->errorInfo(); 
                $this->errorCode =  $sQLErrorInfo[0] . '/' . $sQLErrorInfo[1]; 
                $this->errorMessage = $sQLErrorInfo[2]; 
                $result = FALSE;                     
            } 
        } 
        catch (\PDOException $e) 
        { 
            $this->feedback = 'Er is iets foutgelopen bij het inlezen van de Member tabel.'; 
            $this->errorMessage = 'Fout: ' . $e->getMessage(); 
            $this->errorCode = $e->getCode(); 
            $this->rowCount = -1; 
        } 
        $this->close(); 
        return $result; 
    } 
} 

// Select member by Id
/** 
* Select  
* @return 1 if succeeded, false if not.    
*/
public function selectOne()
	{
		$this->errorMessage = 'none';
		$this->errorCode = 'none';
		$result = FALSE;
		if ($this->connect())
		{
			try
			{
				// Prepare stored procedure call
				$preparedStatement = $this->pdo->
				prepare('CALL MemberSelectOne(:pId)');
				// no getter method, bindParam requires a reference variable
				// if you want to use a method, use then bindValue
				$preparedStatement->bindParam(':pId', $this->id, \PDO::PARAM_INT);
				$preparedStatement->execute();
				// fetch the output
				if ($array = $preparedStatement->fetch(\PDO::FETCH_ASSOC))
				{
					$this->id= $array['Id'];
					$this->userName= $array['UserName'];
					$this->email= $array['Email'];
					$this->password= $array['Password'];
					
					$this->feedback = 'Rij met id <b> ' . $this->getId() . 
						'</b> is gevonden.';
						//Since column in the where must be primary key
						// rowCount() should return 1. Can be used as validation.
				$this->rowCount = $preparedStatement->rowCount();
				$result = $preparedStatement->rowCount();
				}
				else
				{
					$this->feedback = 'Rij met id <b> ' . $this->getId() . 
						'</b> is niet gevonden.';
				}
			}
			catch (\PDOException $e)
			{
				$this->feedback = 'fout => rij is niet gevonden.';
				$this->errorMessage = 'Fout: ' . $e->getMessage();
				$this->errorCode = $e->getCode();
				$this->rowCount = -1;
			}
			$this->close();
		}
		return $result;
	}

// Select member by UserName
/** 
* Select  
* @return array if succeeded, false if not.    
*/
public function SelectByUserName() 
{ 
    $this->errorMessage = 'none'; 
    $this->errorCode = 'none'; 
    $result = FALSE; 
    if ($this->connect()) 
    { 
        try 
        { 
            // Prepare stored procedure call 
            $preparedStatement = $this->pdo-> 
            prepare("CALL MemberSelectByUserName(:pUserName)"); 
            // no getter method, bindParam requires a reference variable 
            // if you want to use a method, use then bindValue 
            $preparedStatement->bindParam(':pUserName', $this->userName, \PDO::PARAM_STR, 80); 
            $preparedStatement->execute(); 
            $this->rowCount = $preparedStatement->rowCount(); 
            // fetch the output 
            if ($result = $preparedStatement->fetchAll()) 
            { 
                $this->feedback .= "{$preparedStatement->rowCount()} rij(en) met $this->userName in tabel Member gevonden."; 
                $array = $result[0]; 
                $this->id= $array['Id']; 
                $this->userName= $array['UserName']; 
                $this->email= $array['Email']; 
            } 
            else 
            { 
                $this->feedback .= "Geen rijen met $this->userName in tabel Member gevonden."; 
                $sQLErrorInfo = $preparedStatement->errorInfo(); 
                $this->errorCode =  $sQLErrorInfo[0] . '/' . $sQLErrorInfo[1]; 
                $this->errorMessage = $sQLErrorInfo[2]; 
                $result = FALSE;                     
            } 
        } 
        catch (\PDOException $e) 
        { 
            $this->feedback = 'Er is iets foutgelopen bij het inlezen van de Member tabel.'; 
            $this->errorMessage = 'Fout: ' . $e->getMessage(); 
            $this->errorCode = $e->getCode(); 
            $this->rowCount = -1; 
        } 
        $this->close(); 
        return $result; 
    } 
} 
}
?>