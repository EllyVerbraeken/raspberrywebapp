<?php
/**
   Testing Warning class
   @created 10 March 2014
   @lastmodified 11 March 2014
   @author Elly Verbraeken
   @version 1.0 
*/
include('../helpers/feedback.class.php');
include('../helpers/validate.class.php'); 
include('base.class.php'); 
include('warning.class.php');
$result="";
$result1="";
$result2="";
$result3="";
$result4="";
$result5="";
$time=date("Y-m-d h:i:s");
$daydeleted = '2014-03-12 15:00:00';
$temperature= 35.06;
$level =3;

$test = new Temperature\Dal\Warning();
$test->setClient(23);
$test->setTemperature($temperature);
$test->setLevel($level);
$test->setTime($time);

$test->insert();
$result .= 'Feedback: ' . $test->getFeedback() . '<br>'; 
$result .= 'Foutmelding: ' . $test->getErrorMessage() . '<br>'; 
$result .= 'Foutcode: ' . $test->getErrorCode() . '<br>';
$result.= 'new id: ' . $test->getId() . '<br>'; 

$test->selectByClient();
$result1 .= 'Feedback: ' . $test->getFeedback() . '<br>'; 
$result1 .= 'Foutmelding: ' . $test->getErrorMessage() . '<br>'; 
$result1 .= 'Foutcode: ' . $test->getErrorCode() . '<br>'; 

$test->selectByLevel();
$result2 .= 'Feedback: ' . $test->getFeedback() . '<br>'; 
$result2.= 'Foutmelding: ' . $test->getErrorMessage() . '<br>'; 
$result2 .= 'Foutcode: ' . $test->getErrorCode() . '<br>'; 

$test->delete($daydeleted);
$result3 .= 'Feedback: ' . $test->getFeedback() . '<br>'; 
$result3 .= 'Foutmelding: ' . $test->getErrorMessage() . '<br>'; 
$result3 .= 'Foutcode: ' . $test->getErrorCode() . '<br>'; 

$test->insert();
$result4 .= 'Feedback: ' . $test->getFeedback() . '<br>'; 
$result4 .= 'Foutmelding: ' . $test->getErrorMessage() . '<br>'; 
$result4 .= 'Foutcode: ' . $test->getErrorCode() . '<br>'; 

$test->deleteByClient();
$result5 .= 'Feedback: ' . $test->getFeedback() . '<br>'; 
$result5 .= 'Foutmelding: ' . $test->getErrorMessage() . '<br>'; 
$result5 .= 'Foutcode: ' . $test->getErrorCode() . '<br>'; 
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <title>Test warning dal</title>
    </head>
    <body>
      <p>Test warning klasse van DAL</p>
        <p>Foutmeldingen</p>
        <p><?php 
           $array = $test->getMessages();
           while (list($key, $val) = each($array)) 
           {
               while (list($keys, $values) = each($val)) 
               {?>
                 <p><?php echo $keys. ' : '.$values.'<br/>';?></p>
                <?php 
               }   
           } ?></p>
        <p>Insert methode</p>
            <?php echo $result;?> 
        <p>Select By Client methode</p>
            <?php echo $result1;?> 
        <p>Select By Level methode</p>
            <?php echo $result2;?> 
        <p>Delete By day methode</p>
        <?php echo $result3;?>  
        <p>Insert methode</p>
            <?php echo $result4;?> 
        <p>Delete By Client methode</p>
        <?php echo $result5;?>   
    </body>
</html>