<?php
    /**
   Warning class
   @created 10 March 2014
   @lastmodified 13 March 2014
   @author Elly Verbraeken
   @version 1.0 
*/
namespace Temperature\Dal;
 class Warning extends \Temperature\Dal\Base 
 {
     /*-----------Declaration of fields----------*/
    private $id; 
    private $client; 
    private $temperature; 
    private $level;
    private $time;

    /*-----------Getters and setters----------*/

    /** 
    * Get Id  
    * @return  the value in $id field    
    */
    public function getId() 
    { 
    return $this->id; 
    } 
    
    /** 
    * Get Client  
    * @return  the value in $client field    
    */
    public function getClient() 
    { 
    return $this->client; 
    } 
    
    /** 
    * Get Temperature  
    * @return  the value in $temperature field    
    */
    public function getTemperature() 
    { 
    return $this->temperature; 
    }

    /** 
    * Get Level  
    * @return  the value in $level field    
    */
    public function getLevel() 
    { 
    return $this->level; 
    }

    /** 
    * Get Time  
    * @return  the value in $time field    
    */
    public function getTime() 
    { 
    return $this->time; 
    }

         /** 
        * Set Id   
        * @return  Bool true if not empty, false if empty.    
        */
    public function setId($value) 
    { 
        if(\Temperature\Helpers\Validate::isEmpty($value))
            {
                $this->messages[] =
                array ('Id' => 'Id is een verplicht veld.');
                $this->isError = TRUE;
                return FALSE;
            }
            else if (!is_numeric($value))
            {
                $this->messages[] =
                array ('Id' => 'Id moet een numerieke waarde hebben.');
                $this->isError = TRUE;
                return FALSE;
            }
            else 
            {
              $value = \Temperature\Helpers\Validate::stripTags($value);
              $this->id = $value; 
              return TRUE;
            }
    } 
       
        /** 
        * Set Client
        * @return  Bool true if not empty, false if empty. 
        */
    public function setClient($value) 
    { 
   if(\Temperature\Helpers\Validate::isEmpty($value))
            {
                $this->messages[] =
                array ('ClientId' => 'ClientId is een verplicht veld.');
                $this->isError = TRUE;
                return FALSE;
            }
            else if (!is_numeric($value))
            {
                $this->messages[] =
                array ('ClientId' => 'ClientId moet een numerieke waarde hebben.');
                $this->isError = TRUE;
                return FALSE;
            }
            else 
            {
              $value = \Temperature\Helpers\Validate::stripTags($value);
              $this->client = $value; 
              return TRUE;
            }
    } 
       
        /** 
        * Set Temperature
        * @return  Bool true if not empty, false if empty.    
        */
    public function setTemperature($value) 
    { 
    if(\Temperature\Helpers\Validate::isEmpty($value))
            {
                $this->messages[] =
                array ('Temperature' => 'Temperture is een verplicht veld.');
                $this->isError = TRUE;
                return FALSE;
            }
            else if (!is_numeric($value))
            {
                $this->messages[] =
                array ('Temperature' => 'Temperature moet een numerieke waarde hebben.');
                $this->isError = TRUE;
                return FALSE;
            }
            else 
            {
              $value = \Temperature\Helpers\Validate::stripTags($value);
              $this->temperature = $value; 
              return TRUE;
            }
    } 
    
        /** 
        * Set Level
        * @return  Bool true if not empty, false if empty.   
        */
    public function setLevel($value) 
    { 
    if(\Temperature\Helpers\Validate::isEmpty($value))
            {
                $this->messages[] =
                array ('Level' => 'Level is een verplicht veld.');
                $this->isError = TRUE;
                return FALSE;
            }
            else if (!is_numeric($value))
            {
                $this->messages[] =
                array ('Level' => 'Level moet een numerieke waarde hebben.');
                $this->isError = TRUE;
                return FALSE;
            }
            else 
            {
              $value = \Temperature\Helpers\Validate::stripTags($value);
              $this->level = $value; 
              return TRUE;
            }
    } 
    
        /** 
        * Set Time
        * @return  Bool true if not empty, false if empty.  
        */
    public function setTime($value) 
    { 
    if(\Temperature\Helpers\Validate::isEmpty($value))
            {
                $this->messages[] =
                array ('Time' => 'Time is een verplicht veld.');
                $this->isError = TRUE;
                return FALSE;
            }
            else 
            {
              $value = \Temperature\Helpers\Validate::stripTags($value);
              $this->time = $value; 
              return TRUE;
            }
    } 
    
    /*-----------CRUD operations----------*/
    /*------------------------------------*/

    /*-----------INSERT----------*/
    /** 
    * Insert   
    * @return  Bool true if succeeded, false if not 
    */
    public function insert()
    { 
        
        if ($this->isError)
		{
			$this->errorMessage = 'Gegevens niet gevalideerd.';
			return FALSE;
		}
    $result = FALSE; 
    $this->errorMessage = 'none'; 
    $this->errorCode = 'none';
    $this->feedback = 'none';
     
    if ($this->connect()) // use Connect methodn of base class
    { 
    try 
    {  $preparedStatement = $this->pdo-> 
    prepare('CALL WarningInsert(@pId, :pClient, :pTemperature, :pLevel, :pTime)');
   
    // bind parameters to fields of Warning class
    $preparedStatement->bindParam(':pClient', $this->client, \PDO::PARAM_INT); 
    $preparedStatement->bindParam(':pTemperature', $this->temperature,\PDO::PARAM_STR, 10); 
    $preparedStatement->bindParam(':pLevel', $this->level, \PDO::PARAM_STR, 10);  
    $preparedStatement->bindParam(':pTime', $this->time, \PDO::PARAM_STR, 10);     
  
    // execute SQL statement
    $preparedStatement->execute(); 
    $this->feedback = "De warning voor {$this->client} is toegevoegd.";
    // fetch the output : result of query : output parameter 
    $this->setId($this->pdo->query('select @pId')->fetchColumn()); 
    //$this->feedback = 'Rij met id <b> ' . $this->GetId() .  
    //'</b> is toegevoegd.'; 
    $result = TRUE; 
    } 
    catch (\PDOException $e) 
    { 
    $this->feedback = "De warning voor {$this->client} is niet toegevoegd."; 
    $this->errorMessage = 'Fout: ' . $e->getMessage(); 
    $this->errorCode = $e->getCode(); 
    } 
    $this->close(); 
    } // end if connect
    return $result; 
    }// end insert

    /*-----------UPDATE----------*/
    // No Update, table filled by PHP

    /*-----------DELETE----------*/
    // Empty table before certain date
    /** 
    * Delete   
    * @return 1 if succeeded, false if not.    
    */
    public function delete($daydelete) 
    { 
        
        if ($this->isError)
		{
			$this->errorMessage = 'Gegevens niet gevalideerd.';
			return FALSE;
		}
    $this->errorMessage = 'none'; 
    $this->errorCode = 'none'; 
    $result = FALSE; 
    if ($this->connect()) 
    { 
    try 
    { 
    // Prepare stored procedure call 
    $preparedStatement = $this->pdo-> 
    prepare('CALL WarningDeleteByDay(:pDay)'); 
   
    $preparedStatement->bindParam(':pDay', $daydelete, \PDO::PARAM_STR, 80); 
    $preparedStatement->execute(); 
    // fetch the output 
    $result = $preparedStatement->rowCount(); 
    if ($result) 
    { 
    $this->feedback = 'Warnings voor ' . $daydelete .  
    ' zijn gedeleted.'; 
    } 
    else
				{
					$this->feedback = 'Warnings voor ' . $daydelete.  
    ' zijn niet gevonden en dus niet gedeleted.';
				}
			}
			catch (\PDOException $e)
			{
				$this->feedback = 'fout => Warnings zijn niet gedeleted.';
				$this->errorMessage = 'Fout: ' . $e->getMessage();
				$this->errorCode = $e->getCode();
			}
			$this->close();
		}
		return $result;
	}

    // Empty table by client
    /** 
    * Delete   
    * @return 1 if succeeded, false if not.    
    */
    public function deleteByClient() 
    { 
        
        if ($this->isError)
		{
			$this->errorMessage = 'Gegevens niet gevalideerd.';
			return FALSE;
		}
    $this->errorMessage = 'none'; 
    $this->errorCode = 'none'; 
    $result = FALSE; 
    if ($this->connect()) 
    { 
    try 
    { 
    // Prepare stored procedure call 
    $preparedStatement = $this->pdo-> 
    prepare('CALL WarningDeleteByclient(:pClient)'); 
   
    $preparedStatement->bindParam(':pClient', $this->client, \PDO::PARAM_INT); 
    $preparedStatement->execute(); 
    // fetch the output 
    $result = $preparedStatement->rowCount(); 
    if ($result) 
    { 
    $this->feedback = "Warnings voor {$this->client} zijn gedeleted."; 
    } 
    else
				{
					$this->feedback = "Warnings voor {$this->client} zijn niet gevonden en dus niet gedeleted.";
				}
			}
			catch (\PDOException $e)
			{
				$this->feedback = 'fout => Warnings zijn niet gedeleted.';
				$this->errorMessage = 'Fout: ' . $e->getMessage();
				$this->errorCode = $e->getCode();
			}
			$this->close();
		}
		return $result;
	}
    /*-----------SELECTByClient----------*/
    // Select warnings of 1 client
    /** 
    * Select   
    * @return array if succeeded, false if not.    
    */
    public function selectByClient()
	{
        
        if ($this->isError)
		{
			$this->errorMessage = 'Gegevens niet gevalideerd.';
			return FALSE;
		}
		$this->errorMessage = 'none';
		$this->errorCode = 'none';
		$result = FALSE;
		if ($this->connect())
		{
			try
			{
				// Prepare stored procedure call
				$preparedStatement = $this->pdo->prepare('CALL WarningSelectByClient(:pClient)');
                $preparedStatement->bindParam(':pClient', $this->client, \PDO::PARAM_INT); 
				$preparedStatement->execute();
				$this->rowCount = $preparedStatement->rowCount();
				if ($result = $preparedStatement->fetchAll())
				{
					$this->feedback = "De warnings van {$this->client} zijn ingelezen.";
				}
				else
				{
					$this->feedback = "De warnings van {$this->client} zijn NIET ingelezen.";
				}
			}
			catch (\PDOException $e)
			{
				$this->feedback = 'Er is iets foutgelopen bij het inlezen van de warnings.';
				$this->errorMessage = 'Fout: ' . $e->getMessage();
				$this->errorCode = $e->getCode();
				$this->rowCount = -1;
			}
			$this->close();
        }
			return $result;
		}

        /*-----------SELECTByLevel----------*/
    // Select warnings by level
    /** 
    * Select   
    * @return array if succeeded, false if not.    
    */

    public function selectByLevel()
	{
        
        if ($this->isError)
		{
			$this->errorMessage = 'Gegevens niet gevalideerd.';
			return FALSE;
		}
		$this->errorMessage = 'none';
		$this->errorCode = 'none';
		$result = FALSE;
		if ($this->connect())
		{
			try
			{
				// Prepare stored procedure call
				$preparedStatement = $this->pdo->prepare('CALL WarningSelectByLevel(:pLevel)');
                $preparedStatement->bindParam(':pLevel', $this->level, \PDO::PARAM_INT); 
				$preparedStatement->execute();
				$this->rowCount = $preparedStatement->rowCount();
				if ($result = $preparedStatement->fetchAll())
				{
					$this->feedback = "De warnings van niveau {$this->level} zijn ingelezen.";
				}
				else
				{
					$this->feedback = "De warnings van niveau {$this->level} zijn NIET ingelezen.";
				}
			}
			catch (\PDOException $e)
			{
				$this->feedback = 'Er is iets foutgelopen bij het inlezen van de warnings.';
				$this->errorMessage = 'Fout: ' . $e->getMessage();
				$this->errorCode = $e->getCode();
				$this->rowCount = -1;
			}
			$this->close();
        }
			return $result;
		}
 }
?>