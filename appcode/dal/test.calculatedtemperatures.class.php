<?php
/**
   Testing CalculatedTemperatures class
   @created 10 March 2014
   @lastmodified 10 March 2014
   @author Elly Verbraeken
   @version 1.0 
*/
include('../helpers/feedback.class.php');
include('../helpers/validate.class.php'); 
include('base.class.php'); 
include('calculatedtemperatures.class.php');

$result="";
$result1="";
$result2="";
$result3="";
$result4="";
$day=date("Y-m-d h:i:s");
$daydeleted = '2014-03-10 15:00:00';
$average= 15.06;
$minimum =14;
$maximum=17.50;
$test = new Temperature\Dal\CalculatedTemperatures();
$test->setClient(5);
$test->setAverage($average);
$test->setMinimum($minimum);
$test->setMaximum($maximum);
$test->setDay($day);

$test->insert();
$result .= 'Feedback: ' . $test->getFeedback() . '<br>'; 
$result .= 'Foutmelding: ' . $test->getErrorMessage() . '<br>'; 
$result .= 'Foutcode: ' . $test->getErrorCode() . '<br>';
$result.= 'new id: ' . $test->getId() . '<br>'; 

$test->selectAverage();
$result1 .= 'Feedback: ' . $test->getFeedback() . '<br>'; 
$result1 .= 'Foutmelding: ' . $test->getErrorMessage() . '<br>'; 
$result1 .= 'Foutcode: ' . $test->getErrorCode() . '<br>'; 

$test->selectMinimum();
$result2 .= 'Feedback: ' . $test->getFeedback() . '<br>'; 
$result2.= 'Foutmelding: ' . $test->getErrorMessage() . '<br>'; 
$result2 .= 'Foutcode: ' . $test->getErrorCode() . '<br>'; 

$test->selectMaximum();
$result3 .= 'Feedback: ' . $test->getFeedback() . '<br>'; 
$result3 .= 'Foutmelding: ' . $test->getErrorMessage() . '<br>'; 
$result3 .= 'Foutcode: ' . $test->getErrorCode() . '<br>'; 

$test->delete($daydeleted);
$result4 .= 'Feedback: ' . $test->getFeedback() . '<br>'; 
$result4 .= 'Foutmelding: ' . $test->getErrorMessage() . '<br>'; 
$result4 .= 'Foutcode: ' . $test->getErrorCode() . '<br>'; 
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <title>Test calculated temperatures dal</title>
    </head>
    <body>
     
        <p>Test calculated temperatures klasse van DAL</p>
        <p>Foutmeldingen</p>
        <p><?php 
           $array = $test->getMessages();
           while (list($key, $val) = each($array)) 
           {
               while (list($keys, $values) = each($val)) 
               {?>
                 <p><?php echo $keys. ' : '.$values.'<br/>';?></p>
                <?php 
               }   
           } ?></p>
        <p>Insert methode</p>
            <?php echo $result;?> 
        <p>Select Average methode</p>
            <?php echo $result1;?> 
        <p>Select Minimum methode</p>
            <?php echo $result2;?> 
        <p>Select Maximum methode</p>
            <?php echo $result3;?> 
        <p>Delete methode</p>
        <?php echo $result4;?> 
    </body>
</html>