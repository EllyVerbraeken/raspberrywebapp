<?php
/**
   TemperatureRaspberry class
   @created 5 March 2014
   @lastmodified 13 March 2014
   @author Elly Verbraeken
   @version 1.0 
*/

namespace Temperature\Dal;
 class TemperatureRaspberry extends \Temperature\Dal\Base 
 {
    /*-----------Declaration of fields----------*/

    private $id; 
    private $client ; 
    private $degree; 
    private $warning;
    private $time ;

    /*-----------Getters and setters----------*/

    /** 
    * Get Id  
    * @return  the value in $id field    
    */
    public function getId() 
    { 
    return $this->id; 
    } 
    
    /** 
    * Get Client  
    * @return  the value in $client field    
    */
    public function getClient() 
    { 
    return $this->client; 
    } 
    
    /** 
    * Get Degree 
    * @return  the value in $degree field    
    */
    public function getDegree() 
    { 
    return $this->degree; 
    } 
    
    /** 
    * Get Warning
    * @return  the value in $warning field    
    */
    public function getWarning() 
    { 
    return $this->warning; 
    } 

    /** 
    * Get Time  
    * @return  the value in $time field    
    */
    public function getTime() 
    { 
    return $this->time; 
    } 

    /** 
    * Set Id   
    * @return  Bool true if not empty, false if empty.    
    */
    public function setId($value) 
    { 
        if(\Temperature\Helpers\Validate::isEmpty($value))
            {
                $this->messages[] =
                array ('Id' => 'Id is een verplicht veld.');
                $this->isError = TRUE;
                return FALSE;
            }
            else if (!is_numeric($value))
            {
                $this->messages[] =
                array ('Id' => 'Id moet een numerieke waarde hebben.');
                $this->isError = TRUE;
                return FALSE;
            }
            else 
            {
              $value = \Temperature\Helpers\Validate::stripTags($value);
              $this->id = $value; 
              return TRUE;
            }
    } 
       
     /** 
     * Set Client
     * @return  Bool true if not empty, false if empty.   
     */
    public function setClient($value) 
    { 
    if(\Temperature\Helpers\Validate::isEmpty($value))
            {
                $this->messages[] =
                array ('ClientId' => 'ClientId is een verplicht veld.');
                $this->isError = TRUE;
                return FALSE;
            }
            else if (!is_numeric($value))
            {
                $this->messages[] =
                array ('ClientId' => 'ClientId moet een numerieke waarde hebben.');
                $this->isError = TRUE;
                return FALSE;
            }
            else 
            {
              $value = \Temperature\Helpers\Validate::stripTags($value);
              $this->client = $value; 
              return TRUE;
            }
    } 
        
      /** 
      * Set Degree  
      * @return  Bool true if not empty, false if empty.  
      */
    public function setDegree($value) 
    { 
        if(\Temperature\Helpers\Validate::isEmpty($value))
            {
                $this->messages[] =
                array ('Degree' => 'Degree is een verplicht veld.');
                $this->isError = TRUE;
                return FALSE;
            }
            else if (!is_numeric($value))
            {
                $this->messages[] =
                array ('Degree' => 'Degree moet een numerieke waarde hebben.');
                $this->isError = TRUE;
                return FALSE;
            }
            else 
            {
                // Business logic for e-line: set warning in degrees:
            if ($value > 28 || $value<10)
            { $this->setWarning('YES');}
            else{ $this->setWarning('NO');}
              $value = \Temperature\Helpers\Validate::stripTags($value);
              $this->degree = $value; 
              return TRUE;
            }
    } 
       
       /** 
       * Set Warning  
       * @return  Bool true if not empty, false if empty.    
       */
    public function setWarning($value) 
    { 
    if(\Temperature\Helpers\Validate::isEmpty($value))
            {
                $this->messages[] =
                array ('Warning' => 'Warning is een verplicht veld.');
                $this->isError = TRUE;
                return FALSE;
            }
            else 
            {
              $value = \Temperature\Helpers\Validate::stripTags($value);
              $this->warning = $value; 
              return TRUE;
            }
    } 
    
     /** 
     * Set Time 
     * @return  Bool true if not empty, false if empty.    
     */
    public function setTime($value) 
    { 
    if(\Temperature\Helpers\Validate::isEmpty($value))
            {
                $this->messages[] =
                array ('Time' => 'Time is een verplicht veld.');
                $this->isError = TRUE;
                return FALSE;
            }
            else 
            {
              $value = \Temperature\Helpers\Validate::stripTags($value);
              $this->time = $value; 
              return TRUE;
            }
    } 

    /*-----------CRUD operations----------*/
    /*------------------------------------*/

    /*-----------INSERT----------*/
    /** 
    * Insert   
    * @return  Bool true if succeeded, false if not.    
    */
    public function insert() 
    { 
        if ($this->isError)
		{
			$this->errorMessage = 'Gegevens niet gevalideerd.';
			return FALSE;
		}
    $result = FALSE; // wachter
    $this->errorMessage = 'none'; 
    $this->errorCode = 'none';
    $this->feedback = 'none';
     
    if ($this->connect()) // use Connect method of base class
    { 
    try 
    { 
    // Prepare stored procedure call  
    // een command object C#
    $preparedStatement = $this->pdo-> 
    prepare('CALL TemperatureRaspberryInsert(@pId, :pClient, :pDegree, :pWarning)');
    //prepare('insert into TemperatureRaspberry'.
    //'(Client, Degree, Warning, Time)'.
    //'values(:pClient, :pDegree, :pWarning, :pTime)');
    
    // bind parameters to fields of TemperatureRaspberry class
      //$preparedStatement->bindParam('@pId', $valueOut, \PDO::PARAM_INT);
    $preparedStatement->bindParam(':pClient', $this->client, \PDO::PARAM_INT); // : : static
    $preparedStatement->bindParam(':pDegree', $this->degree,\PDO::PARAM_STR, 10); 
    $preparedStatement->bindParam(':pWarning', $this->warning, \PDO::PARAM_STR, 10);  
    //$preparedStatement->bindParam(':pTime', $this->time, \PDO::PARAM_STR, 10);   
  
    // execute SQL statement
    $succes = $preparedStatement->execute();
    if( $succes == 1) 
    {
    $this->feedback = "Temperatuur {$this->degree} is toegevoegd.";
    // fetch the output : result of query : output parameter 
    $this->setId($this->pdo->query('select @pId')->fetchColumn()); 
    //$this->feedback = 'Rij met id <b> ' . $this->GetId() .  
    //'</b> is toegevoegd.'; 
    $result = TRUE; 
    }
    else
    {
    $this->feedback = 'Temperatuur is niet toegevoegd.Insert mislukt.';
    $sqlErrorInfo= $preparedStatement->errorInfo();
    $this->errorCode = $sqlErrorInfo[0];
    $this->errorMessage = $sqlErrorInfo[2];
    $result=FALSE;
    }
    } 
    catch (\PDOException $e) 
    { 
    $this->feedback = "Temperatuur {$this->degree} is niet toegevoegd."; 
    $this->errorMessage = 'Fout: ' . $e->getMessage(); 
    $this->errorCode = $e->getCode(); 
    } 
    $this->close(); 
    } // end if connect
    return $result; 
    }// end insert

    /*-----------UPDATE----------*/
    // No Update , temporary table

    /*-----------DELETE----------*/
    // Empty table after processing, by client
    /** 
    * Delete   
    * @return 1 if succeeded, false if not.    
    */
    public function delete() 
    { 
        if ($this->isError)
		{
			$this->errorMessage = 'Gegevens niet gevalideerd.';
			return FALSE;
		}
    $this->errorMessage = 'none'; 
    $this->errorCode = 'none'; 
    $result = FALSE; 
    if ($this->connect()) 
    { 
    try 
    { 
    // Prepare stored procedure call 
    $preparedStatement = $this->pdo-> 
    prepare('CALL TemperatureRaspberryDelete(:pClient)'); 
   
    $preparedStatement->bindParam(':pClient', $this->client, \PDO::PARAM_INT); 
    $preparedStatement->execute(); 
    // fetch the output 
    $result = $preparedStatement->rowCount(); 
    if ($result) 
    { 
    $this->feedback = 'Temperaturen van ' . $this->getClient() .  
    ' zijn gedeleted.'; 
    } 
    else
				{
					$this->feedback = 'Temperaturen van' . $this->getClient() .  
    ' zijn niet gevonden en dus niet gedeleted.';
				}
			}
			catch (\PDOException $e)
			{
				$this->feedback = 'fout => Temperaturen zijn niet gedeleted.';
				$this->errorMessage = 'Fout: ' . $e->getMessage();
				$this->errorCode = $e->getCode();
			}
			$this->close();
		}
		return $result;
	}

    /*-----------SELECT----------*/
    // Select all temperatures of one client
    /** 
    * Select   
    * @return array if succeeded, false if not.    
    */
    public function selectAllTempByClient()
	{
        if ($this->isError)
		{
			$this->errorMessage = 'Gegevens niet gevalideerd.';
			return FALSE;
		}
		$this->errorMessage = 'none';
		$this->errorCode = 'none';
		$result = FALSE;
		if ($this->connect())
		{
			try
			{
				// Prepare stored procedure call
				$preparedStatement = $this->pdo->prepare('CALL TemperatureRaspberrySelect(:pClient)');
                $preparedStatement->bindParam(':pClient', $this->client, \PDO::PARAM_INT); 
				$preparedStatement->execute();
				$this->rowCount = $preparedStatement->rowCount();
				if ($result = $preparedStatement->fetchAll())
				{
					$this->feedback = 'Temperaturen van ' . $this->getClient() .' zijn ingelezen.';
				}
				else
				{
					$this->feedback = 'Temperaturen van ' . $this->getClient() .' zijn NIET ingelezen.';
				}
			}
			catch (\PDOException $e)
			{
				$this->feedback = 'Er is iets foutgelopen bij het inlezen van de temperaturen tabel.';
				$this->errorMessage = 'Fout: ' . $e->getMessage();
				$this->errorCode = $e->getCode();
				$this->rowCount = -1;
			}
			$this->close();
        }
			return $result;
		}

    // Select last inserted temperature for each client
    /** 
    * Select  
    * @return array if succeeded, false if not.    
    */
    public function selectRecentTempByClients()
	{
        if ($this->isError)
		{
			$this->errorMessage = 'Gegevens niet gevalideerd.';
			return FALSE;
		}
		$this->errorMessage = 'none';
		$this->errorCode = 'none';
		$result = FALSE;
		if ($this->connect())
		{
			try
			{
				// Prepare stored procedure call
				$preparedStatement = $this->pdo->prepare('CALL TemperatureRaspberrySelectRecent()');
				$preparedStatement->execute();
				$this->rowCount = $preparedStatement->rowCount();
				if ($result = $preparedStatement->fetchAll())
				{
					$this->feedback = 'Recente temperaturen zijn ingelezen.';
				}
				else
				{
					$this->feedback = 'Recente temperaturen zijn NIET ingelezen.';
				}
			}
			catch (\PDOException $e)
			{
				$this->feedback = 'Er is iets foutgelopen bij het inlezen van de temperaturen tabel.';
				$this->errorMessage = 'Fout: ' . $e->getMessage();
				$this->errorCode = $e->getCode();
				$this->rowCount = -1;
			}
			$this->close();
        }
			return $result;
		}
 }
?>