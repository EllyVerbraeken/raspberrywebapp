<?php
/**
   Controller for Logout
   Redirect to index.php
   @created 7 May 2014
   @lastmodified 8 May 2014
   @author Elly Verbraeken
   @version 1.0 
*/
    include ('../helpers/session.class.php');
    include ('../helpers/membership.class.php');
    include ('../lib/password.php');
    // Our custom secure way of starting a php session. 
    // happens in the constructor of the membership class
    $membership = new \Temperature\Helpers\Membership();
    $membership->LogOut('../../index.php');
?>