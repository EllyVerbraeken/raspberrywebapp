<?php
/**
   Controller for View E-Line-Clients : eline.client.php
   Provide data for chart, response to XML HttpRequest
   @created 4 April 2014
   @lastmodified 15 April 2014
   @author Elly Verbraeken
   @version 1.0 
*/
include ('../helpers/feedback.class.php');
include('../helpers/validate.class.php'); 
include('../dal/base.class.php');  
include('../dal/client.class.php');   
include('../dal/calculatedtemperatures.class.php');     
$temperatures = new Temperature\Dal\CalculatedTemperatures();
$str='';
$client = new Temperature\Dal\Client();   

    if(isset($_POST['choice']) && isset($_POST['ipClient']) && ($_POST['ipClient'] != ''))
    {
         $client->setIpAddress($_POST['ipClient']);
         $client->selectClient();
         $temperatures->setClient($client->getId());
          
        if($_POST['choice'] == 'average')
        { 
            $str =$temperatures->selectAverage();
        }
        else if($_POST['choice'] == 'maximum')
        { 
            $str =$temperatures->selectMaximum();
        }
        else if($_POST['choice'] == 'minimum')
        { 
            $str =$temperatures->selectMinimum();
        }
    }

    else if(isset($_POST['choice']) && isset($_SERVER['REMOTE_ADDR']))
    {
         $client->setIpAddress($_SERVER['REMOTE_ADDR']);
         $client->selectClient();
         $temperatures->setClient($client->getId());
          
        if($_POST['choice'] == 'average')
        { 
            $str =$temperatures->selectAverage();
        }
        else if($_POST['choice'] == 'maximum')
        { 
            $str =$temperatures->selectMaximum();
        }
        else if($_POST['choice'] == 'minimum')
        { 
            $str =$temperatures->selectMinimum();
        }
    }
    else
    {
       $str= 'There was a problem in submitting the data. Please return and try again.';
    }
    //send back
    if ($str == FALSE)
    {
        echo '[{"Average":"0","0":"0","Day":"2014-01-01 01:30:03","1":"2014-01-01 01:30:03"}]';
    }
    else
    { 
      echo json_encode($str);
    }      
?>