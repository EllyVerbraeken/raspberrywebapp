<?php
/**
   Script for calculation of temperatures 1 time a day : CRONJOB!!
   @created 18 March 2014
   @lastmodified 30 March 2014
   @author Elly Verbraeken
   @version 1.0 
*/
include('/home/ellyv/elly.inantwerpen.com/ellyv/appcode/helpers/feedback.class.php'); 
include('/home/ellyv/elly.inantwerpen.com/ellyv/appcode/helpers/validate.class.php'); 
include('/home/ellyv/elly.inantwerpen.com/ellyv/appcode/dal/base.class.php'); 
include('/home/ellyv/elly.inantwerpen.com/ellyv/appcode/dal/temperatureraspberry.class.php');
include('/home/ellyv/elly.inantwerpen.com/ellyv/appcode/dal/client.class.php'); 
include('/home/ellyv/elly.inantwerpen.com/ellyv/appcode/dal/calculatedtemperatures.class.php'); 
$client = new Temperature\Dal\Client();
$raspberry = new Temperature\Dal\TemperatureRaspberry();
$allClients= $client->selectAll();
$total =0;
$count =0;
$maximum =0;
$minimum=100;

foreach ($allClients as $row)
{
    $raspberry->setClient ($row['Id']);
    echo 'Client'. $raspberry->getClient().'<br/>';
    $result = $raspberry->selectAllTempByClient();

    // max temperature for client
    foreach ($result as $temperature)
    {
        echo 'maximum:' .$maximum.'<br/>';
        if($temperature['Degree'] > $maximum)
        { 
            $maximum = $temperature['Degree'];
        }
        echo 'maximum:' .$maximum.'<br/>';
    }
    // min temperature for client
     foreach ($result as $temperature)
    {
        echo 'minimum: '.$minimum .'<br/>';
        if($temperature['Degree'] < $minimum)
        { 
            $minimum = $temperature['Degree'];
        }
        echo 'minimum: '.$minimum .'<br/>';
    }
    // average temperature for client
    foreach ($result as $temperature)
    {
        echo 'total:'.$total.'<br/>';
        $total=$total + $temperature['Degree'];
        $count= $count + 1;    
    }
    $average = $total/$count;
    echo 'average:' .$average.'<br/>';

    // write to table CalculatedTemperatures
    $calculation = new Temperature\Dal\CalculatedTemperatures();
    $calculation->setClient($row['Id']);
    echo 'Client'. $calculation->getClient().'<br/>';
    $calculation->setAverage($average);
    echo 'Average'. $calculation->getAverage().'<br/>';
    $calculation->setMinimum($minimum);
    echo 'Minimum'. $calculation->getMinimum().'<br/>';
    $calculation->setMaximum($maximum);
    echo 'Maximum'. $calculation->getMaximum().'<br/>';

    $query = $calculation->insert();
    if($query === FALSE) 
    {
        echo 'insert mislukt';
    }
    else
    {
       // delete temperatures from table TemperatureRasberry
       $raspberry->delete();
    }
}
?>