<?php
/**
       Test for getdataforchart.php
       @created 7 April 2014
       @lastmodified 7 April 2014
       @author Elly Verbraeken
       @version 1.0 
    */
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <script type="text/javascript" src="../../javascript/ajax.js"></script>
        <title>Testing getdataforchart</title>
        <script>
            var getDataFromServer = function (id,ip) {
                var values = setValues(id,ip);
                var ajax = new Ajax();
                ajax.asynchronousFlag = true;
                ajax.postRequest('getdataforchart.php', values, displayResult);
            }
            // set values
            var setValues = function (id,ip) {
                var values = '';
                values = values + '&choice=' + id;
                if (ip != null && ip != "")
                { values = values + '&ipClient=' + ip; }
                return values;
            }

             var displayResult = function (response) {
                var element = document.getElementById('contentarea');
                element.innerHTML = response;
            }
        </script>
    </head>
    <body>
        <input type="button" id="maximum" onclick="getDataFromServer(this.id,'62.88.55.138')" value="press here"> 
        <div id="contentarea"></div>
    </body>
</html>