<?php
/**
   Controller for Login
   Check for login or set login by post
   @created 7 May 2014
   @lastmodified 8 May 2014
   @author Elly Verbraeken
   @version 1.0 
*/
    include ('appcode/helpers/session.class.php');
    include ('appcode/helpers/membership.class.php');
    include ('appcode/dal/member.class.php');
    include ('appcode/lib/password.php');
    // Our custom secure way of starting a php session. 
    // happens in the constructor of the membership class
    $membership = new \Temperature\Helpers\Membership();
    // check for login
    if(isset($_POST['username'], $_POST['password'])) 
    { 
        $membership->login($_POST['username'], $_POST['password']);    
    }
    else
    {
        $membership->loginCheck();
    }
    $logged = $membership->isLoggedIn();  
?>