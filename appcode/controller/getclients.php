<?php
    /**
       Controller for View E-Line-Clients : eline.client.php
       Selects all clients from table
       @created 8 May 2014
       @lastmodified 8 May 2014
       @author Elly Verbraeken
       @version 1.0 
    */
    $client = new Temperature\Dal\Client();
    $result= $client->selectAll();
?>