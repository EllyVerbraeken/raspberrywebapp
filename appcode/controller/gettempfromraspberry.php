<?php
      /**
       PHP file for receiving data from Raspberry Pi
       @created 17 March 2014
       @lastmodified 9 May 2014
       @author Elly Verbraeken
       @version 1.0 
    */
    include('../helpers/feedback.class.php'); 
    include('../helpers/validate.class.php'); 
    include('../dal/base.class.php'); 
    include('../dal/temperatureraspberry.class.php');
    include('../dal/client.class.php'); 
    $data = new Temperature\Dal\TemperatureRaspberry();
    $client = new Temperature\Dal\Client();
    $count = 0;
    
    while ($count < 10)
    {    
        if(isset($_POST["temperature"]))
        {       
              $client->setIpAddress($_SERVER['REMOTE_ADDR']);
              $client->selectClient();
    
              $temperature=$_POST["temperature"];
              $data->setDegree($temperature);
    
              $data->setClient($client->getId());

              $result = $data->insert();                
        }
        if($result = true)
        {
            break;
        }
        else
        {
            $count++;
        }
    }
    
    if($count > 9)
    {
        // mail error to administrator
        $dataerror = $data->getFeedback().' '.$data->getErrorCode().' '.$data->getErrorMessage();
        $clienterror= $client->getFeedback().' '.$client->getErrorCode().' '.$client->getErrorMessage();
        include ('../lib/class.phpmailer.php');
        $useremail= 'elly.verbraeken@inantwerpen.com';
        $username= 'elly';
        $subject= 'Fout in gettempfromraspberry';
        $message= "Er is een fout opgetreden <br> ";
        $message.= "Fout in client: $clienterror <br>";
        $message.= "Fout in data: $dataerror";

        $mail = new PHPMailer(); // defaults to using php "mail()"
        $mail->Host = "mail.inantwerpen.com"; // SMTP server
        $mail->SMTPAuth = FALSE;                               // Enable SMTP authentication	                
        $mail->Port = 25;   // set the SMTP server port
        $mail->AddReplyTo("elly.verbraeken@inantwerpen.com", 'Elly Verbraeken');
	    $mail->From = "elly.verbraeken@inantwerpen.com";
        $mail->AddAddress($useremail, $username);
        $mail->Subject = $subject;
        $mail->AltBody = "To view the message, please use an HTML compatible email viewer!"; // optional, comment out and test                    
        $mail->isHTML(TRUE);                                  // Set email format to HTML
        $mail->Body = $message;
        if(!$mail->Send()) 
        {
                echo "We konden je geen mail zenden: " . $mail->ErrorInfo;
        } 
        else 
        {
            echo " Je ontvangt van ons nog een e-mail.";
        }
    }
?>