<?php
    /**
       Controller for View E-Line : eline.php
       Selects the recent temperatures for each client
       @created 30 March 2014
       @lastmodified 30 March 2014
       @author Elly Verbraeken
       @version 1.0 
    */
    $test = new Temperature\Dal\TemperatureRaspberry();
    $result= $test->selectRecentTempByClients();   
?>