<?php
    /**
       Test for getdata.php
       @created 17 March 2014
       @lastmodified 17 March 2014
       @author Elly Verbraeken
       @version 1.0 
    */
    include('../helpers/feedback.class.php'); 
    include('../helpers/validate.class.php'); 
    include('../dal/base.class.php'); 
    include('../dal/temperatureraspberry.class.php');
    $test = new Temperature\Dal\TemperatureRaspberry();
    $test->setClient(16);
    $test->setDegree(22.03);
    $test->setWarning('NO');
    $test->insert();
    
    $result= $test->selectRecentTempByClients();
   
    if($result === FALSE) 
    {
        echo 'result is false';
    }
    $date="";
    $degree="";    
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <title>Testing Returning Data</title>
    </head>
    <body>
        <h1>Testing Returning Data from entry in Raspberry table</h1>
        <table>
            <?php      
             foreach ($result as $row)
             {
                $date= $row['TIME'];
                $obj = new DateTime($date);
                $tz = new DateTimeZone('Europe/Brussels');
                $obj->setTimeZone($tz);
                $degree = $row['Degree'];
            ?>
            <tr>
                <td><?php echo $obj->format('Y-m-d H:i:s');;?></td>
                <td><?php echo $degree;?></td>
            </tr>
       <?php } ?>
        </table>

        <p>Foutmeldingen</p>
        <p><?php 
           echo $test->getFeedback();
           $array = $test->getMessages();
           while (list($key, $val) = each($array)) 
           {
               while (list($keys, $values) = each($val)) 
               {?>
                 <p><?php echo $keys. ' : '.$values.'<br/>';?></p>
                <?php 
               }   
           } ?>
        </p>
    </body>
</html>