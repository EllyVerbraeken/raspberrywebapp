<?php
/**
   Testing Validate class
   @created 14 April 2014
   @lastmodified 14 April 2014
   @author Elly Verbraeken
   @version 1.0 
*/
include('feedback.class.php');
include('validate.class.php');
$tekst = 'Hllo <scrip>Boe</script><h1 style = "color:red;">Rode schoenen</h1>';
$tekst = \Temperature\Helpers\Validate::stripTags($tekst);
$email ='sdfj  jlk';
$validemail = \Temperature\Helpers\Validate::email($email);
$empty ='';
$isempty = \Temperature\Helpers\Validate::isEmpty($empty);
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <title></title>
    </head>
    <body>
        <p><?php echo $tekst;?></p>
        <p><?php echo ($validemail ? 'ongeldig email adres' : 'Geldig email');?></p>
        <p><?php echo ($isempty ?  'mag niet leeg zijn': 'Geldig ');?></p>
    </body>
</html>