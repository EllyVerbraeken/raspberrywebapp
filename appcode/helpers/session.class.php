<?php
    namespace Temperature\Helpers;
    class Session 
    {
        protected $name;
        protected $https;
        protected $httpOnly;

        public function start() 
        {
            ini_set('session.use_only_cookies', 1); // Forces sessions to only use cookies. 
            $cookieParams = session_get_cookie_params(); // Gets current cookies params.
            // standaard waarde in php.inin is 0 , cookie wordt verwijderd als browser sluit
            // path is standaard namelijk /
            // domein standaard is leeg = hostnaam van server die cookie maakt
            // secure bepalen we zelf
            // httpOnly bepalen we zelf als we met Javascript aan de sessie kunnen
            //
            session_set_cookie_params($cookieParams["lifetime"], $cookieParams["path"], $cookieParams["domain"], $this->https, $this->httpOnly); 
            session_name($this->name); // Sets the session name to the one set above.
            session_start(); // Start the php session
            session_regenerate_id(); // regenerated the session, delete the old one.  
        }

        public function end()
        {
            // session is started in the constructor

            // Unset all session values
            $_SESSION = array();
            // get session parameters 
            $params = session_get_cookie_params();
            // Delete the actual cookie.
            setcookie(session_name(), '', time() - 42000, 
                $params["path"], 
                $params["domain"], 
                $params["secure"], 
                $params["httpOnly"]);
            // Destroy session
            session_destroy();
        }
    }
?>