<?php
    namespace Temperature\Helpers;

    class Membership extends \Temperature\Helpers\Session
    {
        private $loggedIn;
        private $userName;
 
        public function isAuthenticated()
        {
            return ($this->loggedIn);
        }

        public function isLoggedIn()
        {
            return $this->loggedIn;
        }

        public function getUserName()
        {
            return $this->userName;
        }

        public function getLoginText()
        {
            return ($this->loggedIn ? 'Logout' : 'Login');
        }

        /*
        *   get the link (href) for the login link
        */
        public function getLoginHyperlink()
        {
            return ($this->loggedIn ? 'appcode/controller/logout.php' : 'javascript:void(0)');
        }

        public function __construct() 
        {
            // visitor is not yet authenticated
            $this->loggedIn = FALSE;
            // Set a custom session name, secure membership
            // abbreviated to sec_ms
            $this->name = 'sec_ms'; 
    
            // Set to true if using https.
            $this->https = FALSE; 
            // This stops javascript being able to access the session id.
            $this->httponly = true;  
            $this->start(); 
        }
    
        public function login($userName, $password) 
        {
            $member = new \Temperature\Dal\Member();
            $member->setUserName($userName);
            // see of the user exists
            $member->selectByUserName();
           
            if ($member->getRowCount() == 1)
            {                 
                $member->SelectOne();
                    // Check if the password in the database matches
                    // the password provided by the user.
                    if ($member->verifyPassword($password))
                    { 
                        // Get the user-agent string of the user.
                        $userBrowser = $_SERVER['HTTP_USER_AGENT']; 
                        // XSS protection as we might print this value
                        // Wat blijft er dan nog van over, lege string?
                        // nee, er staat een kapje voor, dit wil zeggen dat 
                        // alle tekens die geen cijfers zijn worden vervangen
                        // door niets!
                        $userId = preg_replace("/[^0-9]+/", "", $member->getId());
                        $_SESSION['userId'] = $userId;
                        // XSS protection as we might print this value
                        $this->username = preg_replace("/[^a-zA-Z0-9_\-]+/", "", $member->getUserName()); 
                        $_SESSION['userName'] = $member->getUserName();
                        $_SESSION['loginString'] = hash('sha512', $member->getPassword() . $userBrowser);
                        
                        // Login successful.
                        $this->loggedIn = TRUE;
                        return true;   
                    } 
                    else 
                    {
                        // Password is not correct                        
                        return false;
                    } 
            }
            else 
            {
                // No user exists. 
                return false;
            } // if row count
        } // login method

        function loginCheck() 
        {
            $this->loggedIn = FALSE;
            // Check if all session variables are set
            if(isset($_SESSION['userId'], $_SESSION['userName'], $_SESSION['loginString'])) 
            {
                $userId = $_SESSION['userId'];
                $loginString = $_SESSION['loginString'];
                $userName = $_SESSION['userName'];
                // Get the user-agent string of the user.
                $userBrowser = $_SERVER['HTTP_USER_AGENT']; // Get the user-agent string of the user.
                $member = new \Temperature\Dal\Member();
                $member->setUserName($userName);
                $member->setId($userId);
                // see of the user exists
                $member->SelectOne();
                 if ($member->GetRowCount() == 1)
                {
                     $loginCheck = hash('sha512', $member->getPassword() . $userBrowser);
                     if($loginCheck == $loginString) 
                     {
                         $this->loggedIn = TRUE;
                         // Logged In
                     } 
                }
            }
            return $this->loggedIn;
        }

        public function logOut($location)
        {
            $this->end();
            header('Location: ' . $location);
        }
    }
?>