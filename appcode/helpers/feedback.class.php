<?php
    namespace Temperature\Helpers;
     /**
     * Feedback class. 
     * @lastmodified 18/03/2014
     * @since 01/06/2013           
     * @author Entreprise de Modes et de Manieres Modernes - e3M       
     * @version 0.1
     */    
    class Feedback
    {
        protected $feedback;
        protected $errorMessage;
        protected $errorCode;
        protected $isError;
        // a dictionary of named messages for feedback
        protected $messages;

        public function __construct()
        {
            $this->errorReset();
        }

        public function getFeedback()
        {
            return $this->feedback;
        }

        public function getErrorMessage()
        {
            return $this->errorMessage;
        }

        public function getErrorCode()
        {
            return $this->errorCode;
        }

        public function getIsError()
        {
            return $this->isError;
        }

        public function getMessages()
        {
            return $this->messages;
        }

        public function errorReset()
        {
            $this->feedback = 'none';
            $this->errorMessage = 'none';
            $this->errorCode = 'none';
            $this->isError = FALSE;
            $this->messages = array();
        }
    }
?>