<?php
  namespace Temperature\Helpers;
       /**
     * Validate class. 
     * @lastmodified 14 April 2014         
     * @author Elly Verbraeken  
     * @version 0.1
     */    
    class Validate 
    {
         /**
     * stripTags Is required for all BLL     
     * @lastmodified 03/04/2014           
     * @author Chris Coyier,
     *      http://css-tricks.com/snippets/php/sanitize-database-inputs/   
     * @version 0.1
     */
        static function stripTags($input) 
        { 
          $search = array(
            '@<script[^>]*?>.*?</script>@si',   // Strip out javascript
            '@<[\/\!]*?[^<>]*?>@si',            // Strip out HTML tags
            '@<style[^>]*?>.*?</style>@siU',    // Strip style tags properly
            '@<![\s\S]*?--[ \t\n\r]*>@'         // Strip multi-line comments
          ); 
            $output = preg_replace($search, '', $input);
            return $output;
        }

         /**
     * email 
     * validates an email address   
     * @lastmodified 04/04/2014        
     * @author W3schools  
     * @version 0.1
     */
        static function email($email)
        {
            if (preg_match("/([\w\-]+\@[\w\-]+\.[\w\-]+)/",$email))
              {
                 return false; 
              }
              else{
                  return TRUE;
              }
        }

         /**
     * isEmpty 
     * checks if ther is at least one character in the string   
     * @lastmodified 04/04/2014          
     * @author W3schools  
     * 
     */
         static function isEmpty($fieldName) 
        { 
            return (!strlen($fieldName) >= 1);
        } 

         /**
     * ip address
     * validates an ip address   
     * @lastmodified 14/04/2014        
     * @author W3schools  
     * @version 0.1
     */
        static function isIP($value)
        {
            if (preg_match("/^(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/",$value))
              {
                 return false; 
              }
              else{
                  return TRUE;
              }
        }

    }
?>