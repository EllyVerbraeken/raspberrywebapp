var datums = [];
            var getDataFromServer = function (id, ip) {
                var values = setValues(id, ip);
                var ajax = new Ajax();
                ajax.asynchronousFlag = true;
                ajax.postRequest('appcode/controller/getdataforchart.php', values, displayResult);
            }
            // set values
            var setValues = function (id, ip) {
                var values = '';
                values = values + '&choice=' + id;
                if (ip != null && ip != "")
                { values = values + '&ipClient=' + ip; }
                return values;
            }

            var displayResult = function (response) {
                var jArray = JSON.parse(response);
                var data = [];
                datums.length = 0;
                var starterY = 0;

                if (jArray[0].Minimum != null) {
                    document.getElementById('title').innerHTML = 'Minimum temperatures';
                    for (var i = 0; i < jArray.length; i++) {
                        var tijdstip = jArray[i].Day;
                        var label = tijdstip.substring(8, 10) + '-' + tijdstip.substring(5, 7);
                        datums.push(label);
                        var temperature = { x: jArray.length - i, y: jArray[i].Minimum };
                        starterY = jArray[i].Minimum;
                        data.push(temperature);
                    }
                }
                else if (jArray[0].Maximum != null) {
                    document.getElementById('title').innerHTML = 'Maximum temperatures';
                    for (var i = 0; i < jArray.length; i++) {
                        var tijdstip = jArray[i].Day;
                        var label = tijdstip.substring(8, 10) + '-' + tijdstip.substring(5, 7);
                        datums.push(label);
                        var temperature = { x: jArray.length - i, y: jArray[i].Maximum };
                        starterY = jArray[i].Maximum;
                        data.push(temperature);
                    }
                }
                else if (jArray[0].Average != null) {
                    document.getElementById('title').innerHTML = 'Average temperatures';
                    for (var i = 0; i < jArray.length; i++) {
                        var tijdstip = jArray[i].Day;
                        var label = tijdstip.substring(8, 10) + '-' + tijdstip.substring(5, 7);
                        datums.push(label);
                        var temperature = { x: jArray.length - i, y: jArray[i].Average };
                        starterY = jArray[i].Average;
                        data.push(temperature);
                    }
                }
                data.push({ x: 0, y: starterY });
                var myLineChart = new LineChart({
                    canvasId: "canvas",
                    minX: 0,
                    minY: 0,
                    maxX: data.length - 1,
                    maxY: 40,
                    unitsPerTickX: 1,
                    unitsPerTickY: 5
                });
                //draw temperatureline
                myLineChart.drawLine(data, "blue", 3, true);

                // draw borders in red line for under-and upper temperature warning zone
                var data = [{ x: 0, y: 10 }, { x: 20, y: 10}];
                myLineChart.drawLine(data, "red", 2, false);
                var data = [{ x: 0, y: 28 }, { x: 20, y: 28}];
                myLineChart.drawLine(data, "red", 2, false);
            }
     