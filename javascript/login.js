function init() {

                if (!document.getElementById('loginanchor').addEventListener)
                { document.getElementById('loginanchor').attachEvent("onclick", openLogin); }
                else
                { document.getElementById('loginanchor').addEventListener("click", openLogin, false); }

                if (!document.getElementById('close').addEventListener)
                { document.getElementById('close').attachEvent("onclick", closeLogin); }
                else
                { document.getElementById('close').addEventListener("click", closeLogin, false); }
            }

            function openLogin(){
                if (!loggedIn) {
                    document.getElementById('loginpopup').style.display = 'block';
                    document.getElementById('fade').style.display = 'block';
                    document.getElementById('username').focus();
                }
            }

            function closeLogin(){
                document.getElementById('loginpopup').style.display='none';
                document.getElementById('fade').style.display='none';
            }