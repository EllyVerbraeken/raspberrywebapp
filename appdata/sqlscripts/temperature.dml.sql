﻿-- elly.inantwerpen.com
-- Created by Elly Verbraeken on 6th of March 2014 
-------------------------------------------
-- MySql DML for TemperatureRaspberry--
-------------------------------------------

-- Insert Stored Procedure for TemperatureRaspberry
USE elly;
DROP PROCEDURE IF EXISTS `TemperatureRaspberryInsert`;
DELIMITER //
CREATE PROCEDURE `TemperatureRaspberryInsert`
(
	OUT pId INT ,
	IN pClientId INT,
    IN pDegree FLOAT,
	IN pWarning VARCHAR (10) CHARACTER SET UTF8 	
)
BEGIN
INSERT INTO `TemperatureRaspberry`
	(
		`TemperatureRaspberry`.`ClientId`,
		`TemperatureRaspberry`.`Degree`,
		`TemperatureRaspberry`.`Warning`,
		`TemperatureRaspberry`.`Time`
	)
	VALUES
	(
		pClientId,
		pDegree,
		pWarning,
		NOW()
    );
	SELECT LAST_INSERT_ID() INTO pId;
END //
DELIMITER ;

-- Delete by Client Stored Procedure for TemperatureRaspberry
USE elly;
DROP PROCEDURE IF EXISTS `TemperatureRaspberryDelete`;
DELIMITER //
CREATE PROCEDURE `TemperatureRaspberryDelete`
(
	IN pClientId INT 
)
BEGIN
delete from TemperatureRaspberry
where `TemperatureRaspberry`.`ClientId` = pClientId;
END //
DELIMITER ;

-- Select all temperatures by Client Stored Procedure for TemperatureRaspberry
USE elly;
DROP PROCEDURE IF EXISTS `TemperatureRaspberrySelect`;
DELIMITER //
CREATE PROCEDURE `TemperatureRaspberrySelect`
(
	IN pClientId INT
)
BEGIN
select `TemperatureRaspberry`.`Degree` from TemperatureRaspberry
where `TemperatureRaspberry`.`ClientId` = pClientId;
END //
DELIMITER ;


-- Select most recent temperature foreach Client Stored Procedure for TemperatureRaspberry
USE elly;
DROP PROCEDURE IF EXISTS `TemperatureRaspberrySelectRecent`;
DELIMITER //
CREATE PROCEDURE `TemperatureRaspberrySelectRecent`
(
)
BEGIN
SELECT ClientId, TIME, Degree, Warning, Name, IPaddress
FROM  `TemperatureRaspberry` 
INNER JOIN  `Client` ON  `TemperatureRaspberry`.ClientId =  `Client`.Id
WHERE TIME
IN (
SELECT MAX( TIME ) 
FROM  `TemperatureRaspberry` 
GROUP BY ClientId
)
ORDER BY Name;
END //
DELIMITER ;

--oude versie:
--SELECT ClientId, Time, Degree, Warning
--FROM  `TemperatureRaspberry` 
--WHERE Time IN (SELECT MAX(Time) FROM `TemperatureRaspberry` 
--GROUP BY ClientId);
-------------------------------------------
-- MySql DML for CalculatedTemperatures--
-------------------------------------------

-- Insert Stored Procedure for CalculatedTemperatures
USE elly;
DROP PROCEDURE IF EXISTS `CalculatedTemperaturesInsert`;
DELIMITER //
CREATE PROCEDURE `CalculatedTemperaturesInsert`
(
	OUT pId INT ,
	IN pClientId INT,
    IN pMinimum FLOAT,
 	IN pMaximum FLOAT,
    IN pAverage FLOAT,
	IN pDay TIMESTAMP
)
BEGIN
INSERT INTO `CalculatedTemperatures`
	(
		`CalculatedTemperatures`.`ClientId`,
		`CalculatedTemperatures`.`Minimum`,
		`CalculatedTemperatures`.`Maximum`,
		`CalculatedTemperatures`.`Average`,
        `CalculatedTemperatures`.`Day`
	)
	VALUES
	(
		pClientId,
		pMinimum,
		pmaximum,
        pAverage,
		pDay
    );
	SELECT LAST_INSERT_ID() INTO pId;
END //
DELIMITER ;

-- Delete by Day Stored Procedure for CalculatedTemperatures
USE elly;
DROP PROCEDURE IF EXISTS `CalculatedTemperaturesDelete`;
DELIMITER //
CREATE PROCEDURE `CalculatedTemperaturesDelete`
(
	IN pDay TIMESTAMP 
)
BEGIN
delete from `CalculatedTemperatures`
where `CalculatedTemperatures`.`Day` < pDay;
END //
DELIMITER ;

-- Select Average by Client Stored Procedure for CalculatedTemperatures
USE elly;
DROP PROCEDURE IF EXISTS `CalculatedTemperaturesSelectAverage`;
DELIMITER //
CREATE PROCEDURE `CalculatedTemperaturesSelectAverage`
(
	IN pClientId INT
)
BEGIN
select `CalculatedTemperatures`.`Average`,`CalculatedTemperatures`.`Day` from CalculatedTemperatures
where `CalculatedTemperatures`.`ClientId` = pClientId
ORDER by `CalculatedTemperatures`.`Day` DESC LIMIT 14;
END //
DELIMITER ;

-- Select Minimum by Client Stored Procedure for CalculatedTemperatures
USE elly;
DROP PROCEDURE IF EXISTS `CalculatedTemperaturesSelectMinimum`;
DELIMITER //
CREATE PROCEDURE `CalculatedTemperaturesSelectMinimum`
(
	IN pClientId INT
)
BEGIN
select `CalculatedTemperatures`.`Minimum`,`CalculatedTemperatures`.`Day` from CalculatedTemperatures
where `CalculatedTemperatures`.`ClientId` = pClientId
ORDER by `CalculatedTemperatures`.`Day` DESC LIMIT 14;
END //
DELIMITER ;

-- Select Maximum by Client Stored Procedure for CalculatedTemperatures
USE elly;
DROP PROCEDURE IF EXISTS `CalculatedTemperaturesSelectMaximum`;
DELIMITER //
CREATE PROCEDURE `CalculatedTemperaturesSelectMaximum`
(
	IN pClientId INT
)
BEGIN
select `CalculatedTemperatures`.`Maximum`,`CalculatedTemperatures`.`Day` from CalculatedTemperatures
where `CalculatedTemperatures`.`ClientId` = pClientId
ORDER by `CalculatedTemperatures`.`Day` DESC LIMIT 14;
END //
DELIMITER ;

-------------------------------------------
-- MySql DML for Warning--
-------------------------------------------

-- Insert Stored Procedure for Warning
USE elly;
DROP PROCEDURE IF EXISTS `WarningInsert`;
DELIMITER //
CREATE PROCEDURE `WarningInsert`
(
	OUT pId INT ,
	IN pClientId INT ,
    IN pTemperature FLOAT,
 	IN pLevel INT,
	IN pTime TIMESTAMP
)
BEGIN
INSERT INTO `Warning`
	(
		`Warning`.`ClientId`,
		`Warning`.`Temperature`,
		`Warning`.`Level`,
        `Warning`.`Time`
	)
	VALUES
	(
		pClientId,
		pTemperature,
		pLevel,
		pTime
    );
	SELECT LAST_INSERT_ID() INTO pId;
END //
DELIMITER ;

-- Delete by Day Stored Procedure for Warning
USE elly;
DROP PROCEDURE IF EXISTS `WarningDeleteByDay`;
DELIMITER //
CREATE PROCEDURE `WarningDeleteByDay`
(
	IN pTime TIMESTAMP 
)
BEGIN
delete from Warning
where `Warning`.`Time` < pTime;
END //
DELIMITER ;

-- Delete by Client Stored Procedure for Warning
USE elly;
DROP PROCEDURE IF EXISTS `WarningDeleteByClient`;
DELIMITER //
CREATE PROCEDURE `WarningDeleteByClient`
(
	IN pClientId INT
)
BEGIN
delete from Warning
where `Warning`.`ClientId` = pClientId;
END //
DELIMITER ;

-- Select Warning by Client Stored Procedure for Warning
USE elly;
DROP PROCEDURE IF EXISTS `WarningSelectByClient`;
DELIMITER //
CREATE PROCEDURE `WarningSelectByClient`
(
	IN pClientId INT
)
BEGIN
select `Warning`.`Level`, `Warning`.`Temperature`, `Warning`.`Time` from Warning
where `Warning`.`ClientId` = pClientId;
END //
DELIMITER ;

-- Select Warning by Level Stored Procedure for Warning
USE elly;
DROP PROCEDURE IF EXISTS `WarningSelectByLevel`;
DELIMITER //
CREATE PROCEDURE `WarningSelectByLevel`
(
	IN pLevel INT
)
BEGIN
select `Warning`.`ClientId`, `Warning`.`Temperature`, `Warning`.`Time` from Warning
where `Warning`.`Level` = pLevel;
END //
DELIMITER ;

-------------------------------------------
-- MySql DML for Client--
-------------------------------------------

-- Insert Stored Procedure for Client
USE elly;
DROP PROCEDURE IF EXISTS `ClientInsert`;
DELIMITER //
CREATE PROCEDURE `ClientInsert`
(
	OUT pId INT ,
	IN pIpAddress VARCHAR (80) CHARACTER SET UTF8 ,
	IN pName VARCHAR (80) CHARACTER SET UTF8 	
)
BEGIN
INSERT INTO `Client`
	(
		`Client`.`Name`,
		`Client`.`IPaddress`
	)
	VALUES
	(
		pName,
		pIpAddress
    );
	SELECT LAST_INSERT_ID() INTO pId;
END //
DELIMITER ;

-- Update Stored Procedure for Client
USE elly;
DROP PROCEDURE IF EXISTS `ClientUpdate`;
DELIMITER //
CREATE PROCEDURE `ClientUpdate`
(
	IN pIpAddress VARCHAR (80) CHARACTER SET UTF8 ,
	IN pName VARCHAR (80) CHARACTER SET UTF8 	
)
BEGIN
UPDATE `Client`
	
		set `Client`.`IPaddress` = pIpAddress
        where `Client`.`Name`= pName;
END //
DELIMITER ;

-- Delete Client by Name Stored Procedure for Client
USE elly;
DROP PROCEDURE IF EXISTS `ClientDelete`;
DELIMITER //
CREATE PROCEDURE `ClientDelete`
(
	IN pName VARCHAR (80) CHARACTER SET UTF8 
)
BEGIN
delete from Client
where `Client`.`Name` = pName;
END //
DELIMITER ;

-- Select all clients Stored Procedure for Client
USE elly;
DROP PROCEDURE IF EXISTS `ClientSelectAll`;
DELIMITER //
CREATE PROCEDURE `ClientSelectAll`
()
BEGIN
select * FROM Client
ORDER BY Name;
END //
DELIMITER ;


-- Select Client by IPaddress Stored Procedure for Client
USE elly;
DROP PROCEDURE IF EXISTS `ClientSelectClient`;
DELIMITER //
CREATE PROCEDURE `ClientSelectClient`
(
IN pIpAddress VARCHAR (80) CHARACTER SET UTF8
)
BEGIN
SELECT Name, Id
FROM  `Client` 
where `Client`.`IPaddress` = pIpAddress;
END //
DELIMITER ;

-- Select Client by Id Stored Procedure for Client
USE elly;
DROP PROCEDURE IF EXISTS `ClientSelectById`;
DELIMITER //
CREATE PROCEDURE `ClientSelectById`
(
IN pId INT
)
BEGIN
SELECT Name, IPaddress
FROM  `Client` 
where `Client`.`Id` = pId;
END //
DELIMITER ;

-------------------------------------------
-- MySql DML for Member--
-------------------------------------------

-- Insert Stored Procedure for Member
USE elly;
DROP PROCEDURE IF EXISTS `MemberInsert`;
DELIMITER //
CREATE PROCEDURE `MemberInsert`
(
	OUT pId INT ,
	IN pUserName VARCHAR (80) CHARACTER SET UTF8 ,
    IN pEmail VARCHAR (80) CHARACTER SET UTF8 , 
	IN pPassword VARCHAR (255) CHARACTER SET UTF8 	
)
BEGIN
INSERT INTO `Member`
	(
		`Member`.`UserName`,
        `Member`.`Email`,
		`Member`.`Password`
	)
	VALUES
	(
		pUserName,
        pEmail,
		pPassword
    );
	SELECT LAST_INSERT_ID() INTO pId;
END //
DELIMITER ;

-- Update Stored Procedure for Member
USE elly;
DROP PROCEDURE IF EXISTS `MemberUpdate`;
DELIMITER //
CREATE PROCEDURE `MemberUpdate`
(
	IN pUserName VARCHAR (80) CHARACTER SET UTF8 ,
    IN pEmail VARCHAR (80) CHARACTER SET UTF8 ,
	IN pPassword VARCHAR (255) CHARACTER SET UTF8 
)
BEGIN
UPDATE `Member`
	
		set `Member`.`Password` = pPassword,
        `Member`.`Email` = pEmail
        where `Member`.`UserName`= pUserName;
END //
DELIMITER ;

-- Delete Member by UserName Stored Procedure for Member
USE elly;
DROP PROCEDURE IF EXISTS `MemberDelete`;
DELIMITER //
CREATE PROCEDURE `MemberDelete`
(
	IN pId INT 
)
BEGIN
delete from Member
where `Member`.`Id` = pId;
END //
DELIMITER ;

-- Select all members Stored Procedure for Member
USE elly;
DROP PROCEDURE IF EXISTS `MemberSelectAll`;
DELIMITER //
CREATE PROCEDURE `MemberSelectAll`
()
BEGIN
select * FROM Member;
END //
DELIMITER ;

-- SelectBy UserName and Password Stored Procedure for Member  
  
USE elly; 
DROP PROCEDURE IF EXISTS `MemberSelectByNameAndPassword`; 
DELIMITER // 
CREATE PROCEDURE `MemberSelectByNameAndPassword` 
( 
IN pUserName VARCHAR (80) CHARACTER SET UTF8 ,
IN pPassword VARCHAR (255) CHARACTER SET UTF8  
) 
BEGIN 
SELECT Id, UserName, Email 
FROM `Member` 
WHERE `Member`.`UserName` = pUserName
AND `Member`.`Password` = pPassword;
END // 
DELIMITER ;

-- SelectOne for Member  
  
USE elly; 
DROP PROCEDURE IF EXISTS `MemberSelectOne`; 
DELIMITER // 
CREATE PROCEDURE `MemberSelectOne` 
( 
IN pId INT
) 
BEGIN 
SELECT Id, UserName, Email, Password
FROM `Member` 
WHERE `Member`.`Id` = pId;
END // 
DELIMITER ;

-- SelectBy UserName Stored Procedure for Member  
  
USE elly; 
DROP PROCEDURE IF EXISTS `MemberSelectByUserName`; 
DELIMITER // 
CREATE PROCEDURE `MemberSelectByUserName` 
( 
IN pUserName VARCHAR (80) CHARACTER SET UTF8  
) 
BEGIN 
SELECT Id, UserName, Email 
FROM `Member` 
WHERE `Member`.`UserName` = pUserName; 
END // 
DELIMITER ;