﻿--- SQL script for inserting clients and Ip addresses

CALL ClientInsert(@pId, '62.88.53.212', 'Elly');
CALL ClientInsert(@pId, '62.22.22.212', 'Daniel');
CALL ClientInsert(@pId, '62.52.62.202', 'Eeklo');

--Updating client (firewall)

CALL ClientUpdate ('81.82.244.143','Elly');

--Inserting data into temperatureraspberry (other user- Danny)

CALL TemperatureRaspberryInsert(@pId, 19, 16, 'NO');
CALL TemperatureRaspberryInsert(@pId, 19, 19, 'NO');
CALL TemperatureRaspberryInsert(@pId, 19, 21, 'NO');
CALL TemperatureRaspberryInsert(@pId, 19, 11, 'NO');

--Inserting data into temperatureraspberry (Elly)

CALL TemperatureRaspberryInsert(@pId, 16, 18, 'NO');
CALL TemperatureRaspberryInsert(@pId, 16, 16, 'NO');
CALL TemperatureRaspberryInsert(@pId, 16, 15, 'NO');
CALL TemperatureRaspberryInsert(@pId, 16, 28, 'NO');

--Inserting data into temperatureraspberry (other user- Eeklo)

CALL TemperatureRaspberryInsert(@pId, 24, 16, 'NO');
CALL TemperatureRaspberryInsert(@pId, 24, 19, 'NO');
CALL TemperatureRaspberryInsert(@pId, 24, 21, 'NO');
CALL TemperatureRaspberryInsert(@pId, 24, 11, 'NO');

--Inserting data into calculatedtemperatures
--Tuinbouw Verhoeven ID 25
CALL CalculatedTemperaturesInsert (@pId,25, 14, 25, 17, '2014-05-07');
--De Landsheer ID 26
CALL CalculatedTemperaturesInsert (@pId,26, 14, 25, 17, '2014-05-03');
CALL CalculatedTemperaturesInsert (@pId,26, 17, 29, 24, '2014-05-04');
CALL CalculatedTemperaturesInsert (@pId,26, 15, 24, 21, '2014-05-05');
CALL CalculatedTemperaturesInsert (@pId,26, 12, 20, 14, '2014-05-06');
CALL CalculatedTemperaturesInsert (@pId,26, 13, 22, 16, '2014-05-07');
CALL CalculatedTemperaturesInsert (@pId,26, 12, 25, 18, '2014-05-08');
CALL CalculatedTemperaturesInsert (@pId,26, 13, 26, 20, '2014-05-09');
CALL CalculatedTemperaturesInsert (@pId,26, 17, 28, 23, '2014-05-10');
CALL CalculatedTemperaturesInsert (@pId,26, 11, 20, 15, '2014-05-11');
--Van Bastelaere ID 27
CALL CalculatedTemperaturesInsert (@pId,27, 10, 24, 18, '2014-05-01');
CALL CalculatedTemperaturesInsert (@pId,27,12, 25, 18 , '2014-05-02');
CALL CalculatedTemperaturesInsert (@pId,27, 13, 22, 16, '2014-05-03');
CALL CalculatedTemperaturesInsert (@pId,27,12, 25, 18 , '2014-05-04');
CALL CalculatedTemperaturesInsert (@pId,27,13, 26, 20 , '2014-05-05');  
CALL CalculatedTemperaturesInsert (@pId,27,17, 28, 23, '2014-05-06');
CALL CalculatedTemperaturesInsert (@pId,27,14, 25, 17 , '2014-05-07');
CALL CalculatedTemperaturesInsert (@pId,27,17, 29, 24 , '2014-05-08');
CALL CalculatedTemperaturesInsert (@pId,27,15, 24, 21 , '2014-05-09');
CALL CalculatedTemperaturesInsert (@pId,27,12, 20, 14 , '2014-05-10');
CALL CalculatedTemperaturesInsert (@pId,27,11, 20, 15, '2014-05-11');
CALL CalculatedTemperaturesInsert (@pId,27, 10, 24, 18, '2014-05-14');
CALL CalculatedTemperaturesInsert (@pId,27,12, 25, 18 , '2014-05-13');
CALL CalculatedTemperaturesInsert (@pId,27, 13, 22, 16, '2014-05-12');
--Elly ID 16
CALL CalculatedTemperaturesInsert (@pId,16, 10, 24, 18, '2014-05-14');
CALL CalculatedTemperaturesInsert (@pId,16,12, 25, 18 , '2014-05-13');
CALL CalculatedTemperaturesInsert (@pId,16, 13, 22, 16, '2014-05-12');
--E-Line KMO ID 29
CALL CalculatedTemperaturesInsert (@pId,29,13, 22, 17 , '2014-05-05');  
CALL CalculatedTemperaturesInsert (@pId,29,14, 23, 18, '2014-05-06');
CALL CalculatedTemperaturesInsert (@pId,29,12, 21, 16 , '2014-05-07');
CALL CalculatedTemperaturesInsert (@pId,29,11, 20, 16 , '2014-05-08');
CALL CalculatedTemperaturesInsert (@pId,29,12, 23, 17 , '2014-05-09');
CALL CalculatedTemperaturesInsert (@pId,29,13, 24, 18 , '2014-05-10');
CALL CalculatedTemperaturesInsert (@pId,29,12, 23, 17, '2014-05-11');
CALL CalculatedTemperaturesInsert (@pId,29, 12, 24, 18, '2014-05-14');
CALL CalculatedTemperaturesInsert (@pId,29,12, 23, 19 , '2014-05-13');
CALL CalculatedTemperaturesInsert (@pId,29, 13, 22, 16, '2014-05-12');
--Guillemaere ID 28
CALL CalculatedTemperaturesInsert (@pId,28,15, 25, 20 , '2014-05-05');  
CALL CalculatedTemperaturesInsert (@pId,28,17, 27, 22, '2014-05-06');
CALL CalculatedTemperaturesInsert (@pId,28,14, 22, 18 , '2014-05-07');
CALL CalculatedTemperaturesInsert (@pId,28,15, 23, 19 , '2014-05-08');
CALL CalculatedTemperaturesInsert (@pId,28,12, 20, 16 , '2014-05-09');
CALL CalculatedTemperaturesInsert (@pId,28,13, 24, 18 , '2014-05-10');
CALL CalculatedTemperaturesInsert (@pId,28,12, 23, 17, '2014-05-11');
CALL CalculatedTemperaturesInsert (@pId,28, 10, 20, 15, '2014-05-14');
CALL CalculatedTemperaturesInsert (@pId,28,12, 23, 19 , '2014-05-13');
