﻿-- elly.inantwerpen.com
-- Created by Elly Verbraeken on 6th of March 2014 
-- 
-- MySql: CREATE TABLE TemperatureRaspberry

USE `elly`;
DROP TABLE IF EXISTS `TemperatureRaspberry`;
CREATE TABLE `TemperatureRaspberry` (
	`Id` INT NOT NULL AUTO_INCREMENT,
	CONSTRAINT PRIMARY KEY(Id),
	`ClientId` INT NOT NULL,
    FOREIGN KEY (ClientId) REFERENCES Client(Id),
	`Warning` VARCHAR (10) CHARACTER SET UTF8 NOT NULL,
	`Degree` FLOAT NOT NULL,
	`Time` TIMESTAMP NOT NULL);

-- MySql: CREATE TABLE CalculatedTemperatures

    USE `elly`;
DROP TABLE IF EXISTS `CalculatedTemperatures`;
CREATE TABLE `CalculatedTemperatures` (
	`Id` INT NOT NULL AUTO_INCREMENT,
	CONSTRAINT PRIMARY KEY(Id),
	`ClientId` INT NOT NULL,
    FOREIGN KEY (ClientId) REFERENCES Client(Id),
		`Minimum` FLOAT NOT NULL,
	`Maximum` FLOAT NOT NULL,
	`Average` FLOAT NOT NULL,
	`Day` TIMESTAMP NOT NULL);

-- MySql: CREATE TABLE Warning
    USE `elly`;
DROP TABLE IF EXISTS `Warning`;
CREATE TABLE `Warning` (
	`Id` INT NOT NULL AUTO_INCREMENT,
	CONSTRAINT PRIMARY KEY(Id),
	`ClientId` INT NOT NULL,
    FOREIGN KEY (ClientId) REFERENCES Client(Id),
		`Temperature` FLOAT NOT NULL,
	`Level` INT NOT NULL,
	`Time` TIMESTAMP NOT NULL);

-- MySql: CREATE TABLE Client
USE `elly`;
DROP TABLE IF EXISTS `Client`;
CREATE TABLE `Client` (
	`Id` INT NOT NULL AUTO_INCREMENT,
	CONSTRAINT PRIMARY KEY(Id),
	`IPaddress` VARCHAR (80) CHARACTER SET UTF8 NOT NULL,
	`Name` VARCHAR (80) CHARACTER SET UTF8 NOT NULL);

-- MySql: CREATE TABLE Member
USE `elly`; 
DROP TABLE IF EXISTS `Member`; 
CREATE TABLE `Member` ( 
`Id` INT NOT NULL AUTO_INCREMENT, 
CONSTRAINT PRIMARY KEY(Id), 
`UserName` VARCHAR (80) CHARACTER SET UTF8 NOT NULL, 
`Password` VARCHAR (255) NOT NULL,
`Email` VARCHAR (80) CHARACTER SET UTF8 NOT NULL,  
CONSTRAINT uc_UserName UNIQUE (UserName)); 