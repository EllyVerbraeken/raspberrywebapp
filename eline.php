<?php
    /**
       User Interface for E-Line : View
       @created 13 March 2014
       @lastmodified 8 May 2014
       @author Elly Verbraeken
       @version 1.0 
    */
    include('appcode/helpers/feedback.class.php');
    include('appcode/helpers/validate.class.php');
    include('appcode/dal/base.class.php'); 
    include('appcode/dal/temperatureraspberry.class.php');
    /* always check if the user is already logged in 
    *  the object $membership is then available 
    *  elsewhere on the page
    */
    //Controller
    include('appcode/controller/controller.php');
    include('login.ui.php');
    if (!$membership->isLoggedIn())
    {
        header('Location: index.php');
    }

?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <!--Page must refresh every 5 minutes-->
        <meta http-equiv="refresh" content="300">
        <meta name="application-name" content="temperature-logging">
        <meta name="description" content="temperature logging in server rooms for monitoring and diagnostics">
        <meta name="keywords" content="temperature logging, server room monitoring">
        <meta name="author" content="Verbraeken Elly">
        <link type="text/css" rel="stylesheet" href="css/raspberry.css">
        <link type="text/css" rel="stylesheet" href="css/eline.ui.css">
        <link type="text/css" rel="stylesheet" href="css/iconfont.css">
        <link type="text/css" rel="stylesheet" href="css/media.css">
        <title>E-Line Temperature Monitoring</title>
        <script>
            var seconds = 300;
            window.onload = function () {
                var countdownTimer = setInterval('secondPassed()', 1000);
            }

            function secondPassed() {
                var minutes = Math.round((seconds - 30) / 60);
                var remainingSeconds = seconds % 60;
                if (remainingSeconds < 10) {
                    remainingSeconds = "0" + remainingSeconds;
                }
                document.getElementById('countdownmin').innerHTML = minutes;
                document.getElementById('countdownsec').innerHTML = remainingSeconds;
                if (seconds == 0) {
                    document.getElementById('countdownmin').innerHTML = minutes;
                    document.getElementById('countdownsec').innerHTML = remainingSeconds;
                }
                else {
                    seconds--;
                }
            }
        </script>
    </head>

    <body>
        <header>
            <div class="room _4x1">
            <a href="index.php" id="home">
            <span class="icon-home"></span>
                    </a></div>
            <div class="room _2x1">
                <h1>E-Line Temperature Monitoring</h1>
            </div>
            <div class="room _4x1">
                <a id="loginanchor" href="<?php echo $membership->GetLoginHyperlink();?>"> 
                    <span class="icon-lock"></span>
                    <span><?php echo $membership->GetLoginText();?></span>
                </a>
            </div>             
        </header>

        <div id="contentarea">
            <div id="clock">         
            <span id="countdownmin" class="timer">5</span>
            <span id="countdown1" class="timer">:</span>
            <span id="countdownsec" class="timer">00</span>
                </div>  
            <span id="countdown">Temperatures will be updated in  </span>
            <table class="features-table">
               <thead>
                <tr>
                    <td>Client</td>
                    <td>IP Address</td>
                    <td>Temperature</td>
                </tr>
               </thead>
               <tbody>
                    <?php                        
                    if( is_array( $result ) && (count($result) > 0) ) 
                    {                            
                        foreach ($result as $row)
                        {
                    ?>
                         <tr>
                            <td><?php echo $row['Name'];?></td>
                            <td><?php echo $row['IPaddress'];?></td>
                    <?php                        
                           if ($row['Warning'] == 'YES')
                           {
                    ?>
                            <td id="warning"><?php echo $row['Degree'];?></td>
                    <?php
                            } 
                           else
                            {
                    ?>
                            <td><?php echo $row['Degree'];?></td>
                    <?php   }
                    ?>
                        </tr>
                    <?php
                        }
                    }
                    ?>
                </tbody>
            </table>
                    <?php if(!$result)
                          { ?>           
                         <div id="feedback">
                             <?php echo $test->getFeedback(). '<br>';
                                   echo $test->getErrorMessage(). '<br>';
                                   echo $test->getErrorCode(). '<br>';?>
                         </div>
                    <?php   }?>
        </div> <!--end content-area-->          
        <footer>
            <div id="vertical"></div>
            <div id="vertical2">
            <p>Copyright E-line KMO</p>
            </div>
            <div id="vertical"></div>
        </footer>
    </body>
</html>